/* simulation_qm
 * Copyright 2018 Manuel Simon
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 * */

/*!
 * This module is built around the 
 * [`CLProgramCollector`](calculator::CLProgramCollector) struct,
 * which offers functions in order to perform several OpenCL kernels
 * and caches compiled verions of the program as well as other
 * expensive-to-construct objects like buffers for interim results.
 * 
 * 
 */

mod configurator {
    /*!
     * This module contains several structs that gather information
     * about a OpenCL-Program or an OpenCL-Buffer that
     * is needed for caching resources when repeatedly doing an OpenCL-Operation
     * on different data.
     */
    use std::hash::{Hash};

    use crate::{DeltaMat, DiagMatConfig};
    /**
     * This struct contains all configuration options that can be applied to main.cl 
     * in order to compile it conditionally.
     */
    #[derive(Clone, PartialEq, Eq, Hash)]
    pub struct ProgramConfig {
        pub deviceid: (u32, String),
        pub vecsize: u8,
        pub dimension: u8,
    }

    /**
     * This struct contians all configuration options that can be applied to 
     * the additum in order to build vecmul_delta kernels.
     */
    #[derive(Clone, PartialEq, Eq, Hash)]
    pub struct VecmulDeltaProgramConfig<T: Clone + PartialEq + Eq + Hash> {
        pub deviceid : (u32, String),
        pub vecsize: u8,
        pub delta: DeltaMat<T>,
    }

    /**
     * This struct contians all configuration options that can be applied to 
     * the additum in order to build vecmul_diag kernels.
     */
    #[derive(Clone, PartialEq, Eq, Hash)]
    pub struct VecmulDiagProgramConfig<T: Clone + PartialEq + Eq + Hash> {
        pub deviceid : (u32, String),
        pub vecsize: u8,
        pub delta: Option<DeltaMat<T>>,
        pub diag: DiagMatConfig,
    }

    #[derive(Clone, PartialEq, Eq, Hash)]
    pub struct BufferScalarprodConfig {
        pub deviceid: (u32, String),
        pub vecsize: u8,
        pub locsize: usize,
        pub loc_stepsize: u16,
    }


    /*/**
     * As [`ocl::SpatialDims`] does not implement the [`Hash`](std::hash::Hash) trait,
     * although it is basically hashable
     * this function provides a way to hash a [`SpatialDims`] in order to be used
     * in structs containing one and wanting to implement `Hash`.
     * 
     * [`SpatiaDims`]: ocl::SpatialDims
     */
    pub fn hash_spatialdims<H: Hasher>(u: SpatialDims, state: &mut H) {
        match u {
            SpatialDims::One(u1) => u1.hash(state),
            SpatialDims::Two(u1, u2) => {
                u1.hash(state);
                u2.hash(state);
            },
            SpatialDims::Three(u1, u2, u3) => {
                u1.hash(state);
                u2.hash(state);
                u3.hash(state);
            },
            SpatialDims::Unspecified => 0usize.hash(state),
        };
    }*/
}
use std::collections::HashMap;
use std::cell::{RefMut, RefCell};
use std::ops::{Deref,};

use ocl::{SpatialDims, Buffer, Program, Kernel};
use ocl::builders::BuildOpt;

use ndarray::Array1;

use num_traits::identities;

use crate::manucl;
use crate::manucl::OpenCLInitialization;
use crate::{DeltaMat, DiagMat, ToCLMaintype};

use self::configurator::{
    ProgramConfig,
    VecmulDeltaProgramConfig, VecmulDiagProgramConfig, 
    BufferScalarprodConfig
};

/**
 * Divides the last component of a [`ocl::SpatialDims`] by divisor. 
 * 
 * This is necessary in order to correct the global size of a kernel like `vecmul_delta`
 * if it is performed in a vectorized way.
 */
fn vecsize_correction(u: SpatialDims, divisor: usize) -> SpatialDims {
    match u {
        SpatialDims::One(u1) => SpatialDims::One(u1/divisor),
        SpatialDims::Two(u1, u2) => SpatialDims::Two(u1, u2/divisor),
        SpatialDims::Three(u1, u2, u3) => SpatialDims::Three(u1, u2, u3/divisor),
        SpatialDims::Unspecified => SpatialDims::Unspecified,
    }
}

/**
 * Names for the kernels that are implemented in main.cl. 
 * 
 * Provides conversion functions from and into the kernel names.
 */
#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub enum LAKernel {
    AddTo,
    ScalarmulTo,
    VecmulDelta,
    VecmulDiag,
    ScalarProdGlob,
    SumupLoc,
    LinearSolveDense,
}

impl LAKernel {
    /**
     * Returns the `String` corresponding to the [`Kernel`](ocl::Kernel).
     */
    pub fn kernel_name(& self) -> String {
        match self {
            LAKernel::AddTo => String::from("add_to"),
            LAKernel::ScalarmulTo => String::from("scalarmul_to"),
            LAKernel::VecmulDelta => String::from("vecmul_delta_kern"),
            LAKernel::VecmulDiag => String::from("vecmul_diag_kern"),
            LAKernel::ScalarProdGlob => String::from("scalar_prod_glob"),
            LAKernel::SumupLoc => String::from("sumup_loc_kern"),
            LAKernel::LinearSolveDense => String::from("linear_solve_dense"),
        }
    }

    /**
     * Returns the `LAKernel` corresponding to `s`.
     * 
     * Expects a valid kernel name
     * 
     * # Panics
     * 
     * If a `String` is given that does not correpsond
     * to a variant of `LAKernel`.
     */
    pub fn from_name(s: & str) -> LAKernel {
        match s {
            "add_to" => LAKernel::AddTo,
            "scalarmul_to" => LAKernel::ScalarmulTo,
            "vecmul_delta" => LAKernel::VecmulDelta,
            "vecmul_diag" => LAKernel::VecmulDiag,
            "scalar_prod_glob" => LAKernel::ScalarProdGlob,
            "sumup_loc_kern" => LAKernel::SumupLoc,
            "linear_solve_dense" => LAKernel::LinearSolveDense,
            _ => panic!("No valid kernel name!")
        }
    }
}


/**
 * The CLProgramCollector provides means of doing repeated Linear Algebra calculations
 * using OpenCL where code is recompiled only when it must be. 
 * 
 * It uses several fields in order to cache compiled sources and buffers
 * for interim results. When a computation is called where the same
 * program can be used (this means in practice that the same compilation options
 * are given), than the compiled binaries are reused -- if not, it is compiled anew. 
 * 
 * The same with buffers: When a computation needs a buffer of the same 
 * size and type as a previous computation, the already created
 * buffer is reused.
 * 
 * It provides methods for basic linear algebra operations
 * like [`vecmul_delta`](CLProgramCollector::vecmul_delta),
 * [`linear_solve_dense`](CLProgramCollector::linear_solve_dense) or 
 * [`scalar_prod`](CLProgramCollector::scalar_prod). 
 * 
 * See [cl-kernels] for detailed documentation of the functions. 
 * 
 * [cl-kernels]: ../../cl-kernels/html/index.html
 * 
 * `Maintype` must be an [`ocl::OclPrm`] in order to be usable in OpenCL
 * Buffers and it must provide the [`ToCLMaintype`](crate::ToCLMaintype) trait,
 * which is easy to implement. Usually `Maintype` will be f32 or f64.
 */
pub struct CLProgramCollector<Maintype: ToCLMaintype + ocl::OclPrm> {
    openclini: OpenCLInitialization,
    program: RefCell<HashMap<ProgramConfig, Program>>,
    buffer_scalarprod: RefCell<HashMap<BufferScalarprodConfig,
                                       (Buffer<Maintype>, Buffer<Maintype>)>>,
    vecmul_delta_program: RefCell<HashMap<VecmulDeltaProgramConfig<Maintype::NonNaN>, Program>>,
    vecmul_diag_program: RefCell<HashMap<VecmulDiagProgramConfig<Maintype::NonNaN>, Program>>,
}

impl<Maintype: ToCLMaintype + ocl::OclPrm> CLProgramCollector<Maintype> {
    /**
     * Constructs a now `CLProgramCollector` with the defalut 
     * [`OpenCLInitialization`](crate::manucl::OpenCLInitialization) `ozenclini`
     * 
     * openclini is then used for all OpenCL operations, like creating programs,
     * and buffers and enqueuing kernels.
     * */
    pub fn new(openclini: OpenCLInitialization) -> CLProgramCollector<Maintype> {
        CLProgramCollector {
            openclini,
            program: RefCell::new(HashMap::new()),
            buffer_scalarprod: RefCell::new(HashMap::new()),
            vecmul_delta_program: RefCell::new(HashMap::new()),
            vecmul_diag_program: RefCell::new(HashMap::new()),
        }
    }
    /**
     * Obtains a regular program from [`program`](CLProgramCollector::program)
     * with compilation options `vecsize` and `dimension` if such one exists.
     * Otherwise, one is created and inserted into `program`.
     * 
     * # See also
     * 
     * [`obtain_vecmul_delta_program`](CLProgramCollector::obtain_vecmul_delta_program) 
     * for obtaining a program with vecmul kernels.
     * */
    fn obtain_program<'a>(
        &'a self,
        vecsize: u8,
        dimension: u8
    ) -> Result<RefMut<'a, Program>, ocl::Error> {
        let p_config = ProgramConfig {
            deviceid: self.openclini.id.clone(),
            vecsize,
            dimension,
        };
        let p = self.program.borrow_mut();
        if p.contains_key(& p_config) {
            Ok(RefMut::map(p, |r| {r.get_mut(&p_config).unwrap()}))
        } else {
             let program = Program::builder()
            .source_file("src/cl/main.cl")
            .devices(self.openclini.stddevice)
            .bo(BuildOpt::cmplr_def(Maintype::to_cl_maintype(), 0i32))
            .bo(BuildOpt::cmplr_def("dimension", i32::from(dimension)))
            .bo(BuildOpt::cmplr_def("VECSIZE", i32::from(vecsize)))
            .build(&self.openclini.stdcontext)?;
            // In debug builds. the compilation log is always printed to stdout.
            #[cfg(test)]
            println!("Build Info: {}", program.build_info(
                self.openclini.stddevice, ocl::enums::ProgramBuildInfo::BuildLog
            ).unwrap());
            Ok(RefMut::map(p, |r| {
                r.entry(p_config).or_insert(program)
            }))
        }
    }

    /**
     * Obtains a vecmul program from [`vecmul_delta_program`](CLProgramCollector::vecmul_delta_program)
     * with compilation options `vecsize` and a hardcoded delta Matrix `delta.
     * Otherwise, one is created and inserted into `vecmul_delta_program`.
     * 
     * The dimension is inferred from `delta`.
     * 
     * `vecmul_delta` and `vecmul_diag` are not implemented as kernel functions
     * in the .cl-source. This is because the delta matrices are given as __constant
     * arguments, so they must be hardcoded into OpenCL code. This function 
     * does this with the given matrix `delta` and creates an kernel
     * that calls vecmul_delta with this exact matrix as argument, then compiles
     * it and returns the result. Similar to regular programs, compiled
     * programs are cached in `vecmul_delta_program`.
     * 
     * # See also
     * 
     * [`obtain_program`](CLProgramCollector::obtain_program) for obtaining a regular program.
     * [`obtain_vecmul_diag_program`](CLProgramCollector::obtain_vecmul_diag_program) 
     * for obtaining a vecmul_diag program.
     * */
    fn obtain_vecmul_delta_program<'a, 'b>(
        &'a self,
        vecsize: u8,
        delta: &'b DeltaMat<Maintype::NonNaN>
    ) -> Result<RefMut<'a, Program>, ocl::Error> {
        let p_config = VecmulDeltaProgramConfig {
            deviceid: self.openclini.id.clone(),
            vecsize,
            delta: delta.clone(),
        };
        let p = self.vecmul_delta_program.borrow_mut();
        if p.contains_key(& p_config) {
            Ok(RefMut::map(p, |r| {r.get_mut(&p_config).unwrap()}))
        } else {
            // Infer the dimension
            let dimension = match delta {
                DeltaMat::One(_) => 1,
                DeltaMat::Two(_) => 2,
                DeltaMat::Three(_) => 3
            };
            /* additum will hold the additional source code containing the hardcoded
             * delta matrix and the final callable vecmul_delta kernel.
             * */
            let mut additum = delta.to_cl_deltamat();
            additum.push('\n');
            additum.push_str(format!("\
            __kernel void vecmul_delta_kern(__global maintype *right, __global maintype *dest) \
            {{ \
            vecmul_delta(m, {}, right, dest); \
            }} \
            ", delta.len()).as_str());
            let program = Program::builder()
                .source_file("src/cl/main.cl")
                .source(additum)
                .devices(self.openclini.stddevice)
                .bo(BuildOpt::cmplr_def(Maintype::to_cl_maintype(), 0i32))
                .bo(BuildOpt::cmplr_def("dimension", i32::from(dimension)))
                .bo(BuildOpt::cmplr_def("VECSIZE", i32::from(vecsize)))
                .build(&self.openclini.stdcontext)?;
            // In debug builds. the compilation log is always printed to stdout.
            #[cfg(test)]
            println!("Build Info: {}", program.build_info(
                self.openclini.stddevice, ocl::enums::ProgramBuildInfo::BuildLog
            ).unwrap());
            Ok(RefMut::map(p, |r| {
                r.entry(p_config).or_insert(program)
            }))
        }
    }

    /**
     * Obtains a vecmul program from [`vecmul_delta_program`](CLProgramCollector::vecmul_delta_program)
     * with compilation options `vecsize` and a hardcoded delta Matrix `delta.
     * Otherwise, one is created and inserted into `vecmul_delta_program`.
     * 
     * The dimension is inferred from `diag`.
     * 
     * `vecmul_delta` and `vecmul_diag` are not implemented as kernel functions
     * in the .cl-source. This is because the delta matrices are given as __constant
     * arguments, so they must be hardcoded into OpenCL code. This function 
     * does this with the given matrices `diag` and `delta` and creates a kernel
     * that calls vecmul_diag with this exact matrix as argument, then compiles
     * it and returns the result. Similar to regular programs, compiled
     * programs are cached in `vecmul_diag_program`.
     * 
     * # See also
     * 
     * [`obtain_program`](CLProgramCollector::obtain_program) for obtaining a regular program.
     * [`obtain_vecmul_delta_program`](CLProgramCollector::obtain_vecmul_delta_program) 
     * for obtaining a vecmul_delta program.
     * */
    fn obtain_vecmul_diag_program<'a, 'b>(
        &'a self,
        vecsize: u8,
        diag: &'b DiagMat<Maintype>,
        delta: Option<DeltaMat<Maintype::NonNaN>>,
    ) -> Result<RefMut<'a, Program>, ocl::Error> {
        let p_config = VecmulDiagProgramConfig {
            deviceid: self.openclini.id.clone(),
            vecsize,
            delta: delta.clone(),
            diag: diag.to_diag_mat_config(),
        };
        let p = self.vecmul_diag_program.borrow_mut();
        if p.contains_key(& p_config) {
            Ok(RefMut::map(p, |r| {r.get_mut(&p_config).unwrap()}))
        } else {
            // Infer the dimension
            let dimension = match diag {
                DiagMat::One(_) => 1,
                DiagMat::Two(_) => 2,
                DiagMat::Three(_) => 3
            };
            match delta {
                Some(DeltaMat::One(_)) => assert_eq!(
                    dimension, 1,
                    "Dimensions of diag and delta do not match!"
                ),
                Some(DeltaMat::Two(_)) => assert_eq!(
                    dimension, 2,
                    "Dimensions of diag and delta do not match!"
                ),
                Some(DeltaMat::Three(_)) => assert_eq!(
                    dimension, 3,
                    "Dimensions of diag and delta do not match!"
                ),
                None => (),
            };
            /* additum will hold the additional source code containing the hardcoded
             * delta matrix, the hardcoded diag matrix that 
             * gets its diags from the arguments (diagargn) to the kernel functions
             * and the final callable vecmul_diag kernel.
             * */
            let (mut additum, deltasize) = match delta {
                Some(d) => (d.to_cl_deltamat(), d.len()),
                None => (String::from("__constant delta *m = 0;"), 0),
            };
            additum.push('\n');
            additum.push_str(diag.to_cl_dimtup().as_str());
            additum.push('\n');
            additum.push_str(format!("\
            __kernel void vecmul_diag_kern({}__global maintype *right, __global maintype *dest)\
            {{\n\
            {}\n\
            vecmul_diag(diagm, m, {}, right, dest);\n\
            }}\n\
            ", diag.to_cl_kernel_args(), diag.create_cl_diagmat(), deltasize).as_str());
            #[cfg(test)]
            println!("{}", additum);
            let program = Program::builder()
                .source_file("src/cl/main.cl")
                .source(additum)
                .devices(self.openclini.stddevice)
                .bo(BuildOpt::cmplr_def(Maintype::to_cl_maintype(), 0i32))
                .bo(BuildOpt::cmplr_def("dimension", i32::from(dimension)))
                .bo(BuildOpt::cmplr_def("VECSIZE", i32::from(vecsize)))
                .build(&self.openclini.stdcontext)?;
            // In debug builds. the compilation log is always printed to stdout.
            #[cfg(test)]
            println!("Build Info: {}", program.build_info(
                self.openclini.stddevice, ocl::enums::ProgramBuildInfo::BuildLog
            ).unwrap());
            Ok(RefMut::map(p, |r| {
                r.entry(p_config).or_insert(program)
            }))
        }
    }

    pub fn add_to<'a>(
        &mut self,
        vecsize: u8,
        globsize: SpatialDims,
        locsize: SpatialDims,
        left: & Buffer<Maintype>,
        right: &'a Buffer<Maintype>
    ) -> Result<&'a Buffer<Maintype>, ocl::Error> {
        // The methods calling OpenCL kernels all have the general common 
        // structure: 
        /* At first we check the given parameters for validity
         * – Recall that calling the kernel is an unsafe function, so we
         * don’t want it to fail. Maybe in the future we will return an error
         * instead of panicking.
         * */
        assert_eq!(
            manucl::total_spatial_dims(globsize), left.len(),
            "Buffers must have length specified in globsize!"
        );
        assert_eq!(
            manucl::total_spatial_dims(globsize), right.len(),
            "Buffers must have length specified in globsize!"
        );
        assert_eq!(
            manucl::last_spatial_dim(globsize) % vecsize as usize, 0usize,
            "Size in last dimension must be a multiple of vecsize!"
        );
        assert_eq!(
            globsize.dim_count(), locsize.dim_count(),
            "globsize and locsize must have same dimension!"
        );
        let dimension: u8 = globsize.dim_count() as u8;
        // Then we obtain the program – if possible from our cache.
        let mainprogram = self.obtain_program(
            vecsize,
            dimension,
        )?;

        // Create the kernel.
        // Maybe in the future kernels will also be cached
        // This did not work yet because KernelBuilder demands all arguments
        // to be set when it is created and I was not able to figure out
        // how to set a local argument on a Kernel without using deprecated
        // functions (only how to set it on a KernelBuilder). 
        // In the meantime I have found out that the first is not really
        // a problem, as you can pass a None as the argument to the KernelBuilder.
        let kern = Kernel::builder()
            .program(mainprogram.deref())
            .name(LAKernel::AddTo.kernel_name())
            .queue(self.openclini.stdqueue.clone())
            .global_work_size(vecsize_correction(globsize, usize::from(vecsize)))
            .local_work_size(locsize)
            .arg(left)
            .arg(right)
            .build()?;

        // Finally the kernel is enqueued and waited for to finish.
        unsafe {
            kern.enq()?;
            if let Some(q) = kern.default_queue() {
                q.finish()?;
            }
        }

        Ok(right)
    }

    pub fn scalarmul_to<'a>(
        &mut self,
        vecsize: u8,
        globsize: SpatialDims,
        locsize: SpatialDims,
        factor: Maintype,
        right: &'a Buffer<Maintype>
    ) -> Result<&'a Buffer<Maintype>, ocl::Error> {
        // For commentary see add_to
        assert_eq!(
            manucl::total_spatial_dims(globsize), right.len(),
            "Buffers must have length specified in globsize!"
        );
        assert_eq!(
            manucl::last_spatial_dim(globsize) % vecsize as usize, 0usize,
            "Size in last dimension must be a multiple of vecsize!"
        );
        assert_eq!(
            globsize.dim_count(), locsize.dim_count(),
            "globsize and locsize must have same dimension!"
        );
        let dimension: u8 = globsize.dim_count() as u8;

        let mainprogram = self.obtain_program(
            vecsize,
            dimension,
        )?;

        let kern = Kernel::builder()
            .program(mainprogram.deref())
            .name(LAKernel::ScalarmulTo.kernel_name())
            .queue(self.openclini.stdqueue.clone())
            .global_work_size(vecsize_correction(globsize, usize::from(vecsize)))
            .local_work_size(locsize)
            .arg(factor)
            .arg(right)
            .build()?;

        unsafe {
            kern.enq()?;
            if let Some(q) = kern.default_queue() {
                q.finish()?;
            }
        }

        Ok(right)
    }

    pub fn vecmul_delta(
        &mut self,
        left: & DeltaMat<Maintype::NonNaN>,
        right: & Buffer<Maintype>,
        dest: & Buffer<Maintype>,
        globsize: SpatialDims,
        locsize: SpatialDims,
        vecsize: u8,
    ) -> Result<(), ocl::Error> {
        // For commentary see add_to.
        assert_eq!(
            manucl::total_spatial_dims(globsize), right.len(),
            "Buffers must have length specified in globsize!"
        );
        assert_eq!(
            manucl::last_spatial_dim(globsize) % vecsize as usize, 0usize,
            "Size in last dimension must be a multiple of vecsize!"
        );
        assert_eq!(
            globsize.dim_count(), left.dim_count(),
            "globsize and left must have same dimension!"
        );
        assert_eq!(
            locsize.dim_count(), left.dim_count(),
            "locsize and left must have same dimension!"
        );
        let program = self.obtain_vecmul_delta_program(vecsize, left)?;
        let kern = Kernel::builder()
            .program(program.deref())
            .name(LAKernel::VecmulDelta.kernel_name())
            .queue(self.openclini.stdqueue.clone())
            .global_work_size(vecsize_correction(globsize, vecsize as usize))
            .local_work_size(locsize)
            .arg(right)
            .arg(dest)
            .build()?;

        unsafe {
            kern.enq()?;
            if let Some(q) = kern.default_queue() {
                q.finish()?;
            }
        }

        Ok(())
    }

    pub fn vecmul_diag(
        &mut self,
        left: & DiagMat<Maintype>,
        leftdelta: Option<DeltaMat<Maintype::NonNaN>>,
        right: & Buffer<Maintype>,
        dest: & Buffer<Maintype>,
        globsize: SpatialDims,
        locsize: SpatialDims,
        vecsize: u8,
    ) -> Result<(), ocl::Error> {
        // For commentary see add_to.
        assert_eq!(
            manucl::total_spatial_dims(globsize), right.len(),
            "Buffers must have length specified in globsize!"
        );
        assert_eq!(
            manucl::last_spatial_dim(globsize) % vecsize as usize, 0usize,
            "Size in last dimension must be a multiple of vecsize!"
        );
        assert_eq!(
            globsize.dim_count(), left.dim_count(),
            "globsize and left must have same dimension!"
        );
        assert_eq!(
            locsize.dim_count(), left.dim_count(),
            "locsize and left must have same dimension!"
        );
        let program = self.obtain_vecmul_diag_program(vecsize, left, leftdelta)?;
        let mut kernbuilder = Kernel::builder();
        kernbuilder.program(program.deref())
            .name(LAKernel::VecmulDiag.kernel_name())
            .queue(self.openclini.stdqueue.clone())
            .global_work_size(vecsize_correction(globsize, vecsize as usize))
            .local_work_size(locsize);

        for bt in left.buffers() {
            kernbuilder.arg(bt);
        }
        let kern = kernbuilder.arg(right)
            .arg(dest)
            .build()?;

        unsafe {
            kern.enq()?;
            if let Some(q) = kern.default_queue() {
                q.finish()?;
            }
        }

        Ok(())
    }

    pub fn linear_solve_dense(
        &mut self,
        mat: & Buffer<Maintype>,
        right: & Buffer<Maintype>,
        dest: & Buffer<Maintype>,
        globsize: (usize, usize),
        vecsize: u8,
    ) -> Result<(), ocl::Error> {
        // For commentary see add_to.
        assert_eq!(
            mat.len(), right.len()*right.len(),
            "Dimensions of mat and right must match!"
        );
        let n = right.len();

        let program = self.obtain_program(vecsize, 2)?;
        let kern = Kernel::builder()
            .program(program.deref())
            .name(LAKernel::LinearSolveDense.kernel_name())
            .queue(self.openclini.stdqueue.clone())
            .global_work_size(SpatialDims::from(globsize))
            .local_work_size(SpatialDims::from(globsize))
            .arg(n as u16)
            .arg(mat)
            .arg(right)
            .arg(dest)
            .arg_local::<Maintype>(n*n)
            .arg_local::<Maintype>(n)
            .build()?;

        unsafe {
            kern.enq()?;
            if let Some(q) = kern.default_queue() {
                q.finish()?;
            }
        }

        Ok(())
    }
}

impl<Maintype: ToCLMaintype + ocl::OclPrm + identities::Zero> CLProgramCollector<Maintype> {
    /**
     * Computes the scalar product of the two OpenCL [Buffers](ocl::Buffer) `left` and `right`. 
     * 
     * #Arguments
     * 
     * `openclini` An [`OpenCLInitialization`] holding a context and a standard device.
     * 
     * `left` – An [`ocl::Buffer`] holding the left side of the scalar product. 
     * 
     * `right` – An [`ocl::Buffer`] holding the right side of the scalar product. 
     * 
     * `locize` – The local sizes used for the first
     * and the second part of the calculation. 
     * 
     * `loc_stepsize` – The number of additions that shall be performed in each reduction round
     * of the Addition of all single products to a single value. `(2, 2)` is a recommended value.
     * Must be greater than one
     * 
     * `vecsize` – The vecsizes used for the first and the second part of the calculation. 
     * For the first part, the preferred vector width of the device is recommended. For the second
     * part, a small value should be chosen, like `1` or `2`. 
     * Must be `1`, `2`, `4`, `8` or `16`
     *  
     * # Errors
     * 
     * Every error of an ocl-function that is called is passed on. 
     * Furthermore it might happen that writing to the [ndarrays](ndarray::ArrayBase)
     * used in the function
     * fails, which is also passed on. 
     * 
     * # Panics
     * 
     * If `left` and `right` do not have the same size.
     * 
     * If invalid values are given for `loc_stepsize` or `vecsize`.
     * 
     * [`OpenCLInitialization`]: crate::manucl::OpenCLInitialization
     * */
    pub fn scalar_prod(
        &mut self,
        left: & Buffer<Maintype>, right: & Buffer<Maintype>,
        locsize: (usize, usize), loc_stepsize: (u16, u16), vecsize: (u8, u8)
    ) -> Result<Maintype, ocl::error::Error> {
        // For a great part of the commentary see add_to.
        assert_eq!(left.len(), right.len(),
            "Buffers left and right must have the same size!");
        assert!(vecsize.0 == 1 || vecsize.0 == 2 || vecsize.0 == 4
            || vecsize.0 == 8 || vecsize.0 == 16,
            "Both components of vecsize must be 1, 2, 4, 8 or 16!");
        assert!(vecsize.1 == 1 || vecsize.1 == 2 || vecsize.1 == 4
            || vecsize.1 == 8 || vecsize.1 == 16,
            "Both components of vecsize must be 1, 2, 4, 8 or 16!");
        assert!(loc_stepsize.0 > 1 && loc_stepsize.1 > 1,
            "Both components of locsize must be greater than one.");
        let dimension: u8 = 1;

        /* The scalar prod functions uses two kernels: One that is executed
         * using many work groups and computes the products of the components
         * add performs the bigger part of the additive reduction
         * and one that is executed with only one work group and does
         * the rest of the additive reduction. 
         * 
         * This is necessary becaus there is no synchronisation between work groups,
         * so in the firs kernel, every work group can only produce one
         * output value and then finish, because there isn’t more to reduce. 
         * 
         * Those interim results must be saved in a buffer and this is wiht iresbuffer
         * is for.
         * 
         * Furthermore, resbuffer is a one-element-buffer that will hold the final result
         * – this is the only way how a kernel can return values.
         * 
         * Like programs, both buffers are cached.
         * */
        let mut b = self.buffer_scalarprod.borrow_mut();
        let (iresbuffer, resbuffer): & (Buffer<Maintype>, Buffer<Maintype>) = b.entry(
            BufferScalarprodConfig {
                deviceid: self.openclini.id.clone(),
                vecsize: vecsize.1,
                locsize: locsize.1,
                loc_stepsize: loc_stepsize.1,
            }
        ).or_insert_with(|| {(
            Buffer::<Maintype>::builder()
                .len(SpatialDims::One(locsize.1*((loc_stepsize.1*(vecsize.1 as u16)) as usize)))
                .queue(self.openclini.stdqueue.clone())
                //DANGER OF PANIC!
                .build().unwrap(),
            Buffer::<Maintype>::builder()
                .len(SpatialDims::One(1))
                .queue(self.openclini.stdqueue.clone())
                //DANGER OF PANIC!
                .build().unwrap()
        )});

        /* Because there are to kernels to execute, program must be borrowed twice. 
         * Both (mutable!) borrows must be put into different scopes in order
         * to obey the borrowing rules. 
         * */
        {
            let mainprogram = self.obtain_program(
                vecsize.0,
                dimension,
            )?;

            let locarg = locsize.0*((loc_stepsize.0*(vecsize.0 as u16)) as usize);
            let globkern = Kernel::builder()
                .program(mainprogram.deref())
                .name(LAKernel::ScalarProdGlob.kernel_name())
                .queue(self.openclini.stdqueue.clone())
                .global_work_size(SpatialDims::One(
                    locsize.0*locsize.1*(((vecsize.1 as u16)*loc_stepsize.1) as usize)
                ))
                .local_work_size(SpatialDims::One(locsize.0))
                .arg(left.len())
                .arg(loc_stepsize.0)
                .arg(left)
                .arg(right)
                .arg(iresbuffer)
                .arg_local::<Maintype>(locarg)
                .build()?;

            unsafe {
                globkern.enq()?;
                if let Some(q) = globkern.default_queue() {
                    q.finish()?;
                }
            }
        }

        {
            let sumupprogram = self.obtain_program(
                vecsize.1,
                dimension,
            )?;

            let sumupkern = Kernel::builder()
                .program(sumupprogram.deref())
                .name(LAKernel::SumupLoc.kernel_name())
                .queue(self.openclini.stdqueue.clone())
                .global_work_size(SpatialDims::One(locsize.1))
                .local_work_size(SpatialDims::One(locsize.1))
                .arg(loc_stepsize.1)
                .arg(iresbuffer)
                .arg(resbuffer)
                .arg_local::<Maintype>(locsize.1*((loc_stepsize.1*(vecsize.1 as u16)) as usize))
                .build()?;

            unsafe {
                sumupkern.enq()?;
                if let Some(q) = sumupkern.default_queue() {
                    q.finish()?;
                }
            }
        }

        let mut res: Array1<Maintype> = Array1::zeros(1);
        // Should be okay because as res is freshly created (and punctual)
        // it should always have standard layout.
        crate::manucl::read_buffer(resbuffer, &mut res)?;
        
        Ok(res[0])
    }
}

#[cfg(test)]
mod tests {
    use crate::manucl::OpenCLInitialization;
    use ocl::SpatialDims;
    use ocl::{Buffer, Program, Kernel};
    use ocl::builders::BuildOpt;
    use ndarray::{Array1, Array2};
    use crate::{DeltaMat, DiagMat};
    use crate::ToCLMaintype;
    use crate::manucl;
    use crate::tests::default_openclini;
    use super::{CLProgramCollector};
    use noisy_float::types::{R32};
    use assert_float_eq::{afe_abs, afe_is_relative_eq,  expect_float_relative_eq};
    use rand::{distributions, distributions::Distribution};
    macro_rules! assert_float_relative_eq_m {
        (&left:expr, $right:expr, $epsilon:expr) => ({
            let (left_val, right_val, epsilon) = ($left, $right, $epsilon);
            if !expect_float_relative_eq!(left_val, right_val, epsilon).is_ok() {
                panic!(r#"assertion failed: `(left == right)`
  left: `{:?}`,
 right: `{:?}`: {}"#, left_val, right_val,)
            }
        });
        ($left:expr, $right:expr, $epsilon:expr, $($arg:tt)+) => ({
            let (left_val, right_val, epsilon) = ($left, $right, $epsilon);
            if !expect_float_relative_eq!(left_val, right_val, epsilon).is_ok() {
                panic!(r#"assertion failed: `(left == right)`
  left: `{:?}`,
 right: `{:?}`: {}"#, left_val, right_val,
                           format_args!($($arg)+))
            }
        })
    }

    fn fill_one_d<E: ocl::OclPrm + ToCLMaintype>(
        openclini: & OpenCLInitialization,
        data: &mut Array1<E>, buf: & Buffer<E>,
        locsize: SpatialDims
    ) {
        let mainprogram = Program::builder()
            .source_file("src/cl/test.cl")
            .devices(openclini.stddevice)
            .bo(BuildOpt::cmplr_def(E::to_cl_maintype(), 0i32))
            .build(&openclini.stdcontext)
            .unwrap();
        
        let b = &*buf;
        let kern = Kernel::builder()
            .program(&mainprogram)
            .name("fill_one_d")
            .queue(openclini.stdqueue.clone())
            .global_work_size(SpatialDims::One(buf.len()))
            .local_work_size(locsize)
            .arg(b)
            .build()
            .expect("Failed to load test kernel");

        unsafe {
            kern.enq().unwrap();
        }  

        if let Some(q) = kern.default_queue() {
            q.finish().unwrap();
        } else {
            panic!("Could not obtain default queue");
        }

        manucl::read_buffer(buf, data).unwrap();
    }

    fn fill_two_d<E: ocl::OclPrm + ToCLMaintype>(
        openclini: & OpenCLInitialization,
        data: &mut Array2<E>, buf: & Buffer<E>,
        globsize: (usize, usize),
        locsize: SpatialDims
    ) {
        assert_eq!(
            globsize.0*globsize.1, buf.len(),
            "Buffer size must match given global size"
        );
        let mainprogram = Program::builder()
            .source_file("src/cl/test.cl")
            .devices(openclini.stddevice)
            .bo(BuildOpt::cmplr_def(E::to_cl_maintype(), 0i32))
            .build(&openclini.stdcontext)
            .unwrap();
        
        let b = &*buf;
        let kern = Kernel::builder()
            .program(&mainprogram)
            .name("fill_two_d")
            .queue(openclini.stdqueue.clone())
            .global_work_size(SpatialDims::from(globsize))
            .local_work_size(locsize)
            .arg(b)
            .build()
            .expect("Failed to load test kernel");

        unsafe {
            kern.enq().unwrap();
        }  

        if let Some(q) = kern.default_queue() {
            q.finish().unwrap();
        } else {
            panic!("Could not obtain default queue");
        }

        manucl::read_buffer(buf, data).unwrap();
    }

    fn default_locsize(_openclini: & OpenCLInitialization, dimension: u8) -> SpatialDims {
        match dimension {
            1 => SpatialDims::One(256usize),
            2 => SpatialDims::Two(16, 16),
            _ => panic!("Give dimension 1 or 2!"),
        }
    }

    const DEFAULT_VECSIZE: u8 = 8;
    const DEFAULT_PREC: f32 = 1./(0x100000 as f32);

    #[test]
    // Ignored because it fails if not run with --test-threads 1
    #[ignore]
    pub fn vecmul_delta_one_dim() {
        let openclini = default_openclini()
            .expect("Could not create OpenCLInitialization");
        let hsize = 2048usize;
        let locsize = default_locsize(&openclini, 1);
        let vecsize = DEFAULT_VECSIZE;
        let mut data: Array1<f32>;
        let mut datadest: Array1<f32>;
        unsafe {
            data = Array1::uninitialized(hsize);
            datadest = Array1::uninitialized(hsize);
        }
        let srcbuffer = manucl::buffer_from_ndarray(&data, &openclini).unwrap();
        let destbuffer = manucl::buffer_from_ndarray(&datadest, &openclini).unwrap();
        fill_one_d(&openclini, &mut data, &srcbuffer, locsize);

        let mut calc = CLProgramCollector::<f32>::new(openclini);
        
        let del = DeltaMat::One(
            vec![(R32::new(-3.0), (-1,)), (R32::new(2.0), (0,)), (R32::new(-1.0), (1,)), (R32::new(1.5), (2,))]
        );
        calc.vecmul_delta(
            &del,
            &srcbuffer, &destbuffer, SpatialDims::One(hsize), 
            locsize, vecsize
        ).unwrap();
        manucl::read_buffer(&destbuffer, &mut datadest).unwrap();

        let prec = DEFAULT_PREC;
        let mut i;
        for ipre in 1..15 {
            i = ipre*hsize/16;
            assert_float_relative_eq_m!(
                datadest[i],
                - 3f32*data[i-1] + 2f32*data[i] - data[i+1] + 1.5f32*data[i+2],
                prec,
                "Wrong result at {} (regular case behaviour)", i
            );
        }

        for ipre in 1..15 {
            i = ipre*hsize/16+3;
            assert_float_relative_eq_m!(
                datadest[i],
                - 3f32*data[i-1] + 2f32*data[i] - data[i+1] + 1.5f32*data[i+2],
                prec,
                "Wrong result at {} (odd case behaviour)", i
            );
        }

        i = 0;
        assert_float_relative_eq_m!(
            datadest[i],
            2f32*data[i] - data[i+1] + 1.5f32*data[i+2],
            prec,
            "Wrong result at {} (corner case behaviour)", i
        );
        i = hsize-1;
        assert_float_relative_eq_m!(
            datadest[i],
            - 3f32*data[i-1] + 2f32*data[i],
            prec,
            "Wrong result at {} (regular case behaviour)", i
        );
        i = 1;
        assert_float_relative_eq_m!(
            datadest[i],
            - 3f32*data[i-1] + 2f32*data[i] - data[i+1] + 1.5f32*data[i+2],
            prec,
            "Wrong result at {} (regular case behaviour)", i
        );
        i = hsize-2;
        assert_float_relative_eq_m!(
            datadest[i],
            - 3f32*data[i-1] + 2f32*data[i] - data[i+1],
            prec,
            "Wrong result at {} (regular case behaviour)", i
        );
    }

    #[test]
    // Ignored because it fails if not run with --test-threads 1
    #[ignore]
    pub fn vecmul_delta_two_dim() {
        let openclini = default_openclini()
            .expect("Could not create OpenCLInitialization");
        let hsize = 2048usize;
        let vsize = 2048usize;
        let locsize = default_locsize(&openclini, 2);
        let vecsize = DEFAULT_VECSIZE;
        let mut data: Array2<f32>;
        let mut datadest: Array2<f32>;
        unsafe {
            data = Array2::uninitialized((hsize, vsize));
            datadest = Array2::uninitialized((hsize, vsize));
        }
        let srcbuffer = manucl::buffer_from_ndarray(&data, &openclini).unwrap();
        let destbuffer = manucl::buffer_from_ndarray(&datadest, &openclini).unwrap();
        fill_two_d(&openclini, &mut data, &srcbuffer, (hsize, vsize), locsize);

        let mut calc = CLProgramCollector::<f32>::new(openclini);
        
        let del = DeltaMat::Two(vec![
            (R32::new(1.5), (-2, -17)), (R32::new(-0.25), (-2, -16)),
            (R32::new(-3.0), (-1, -1)), (R32::new(2.0), (-1, 0)), (R32::new(1.5), (-1, 1)),
            (R32::new(-1.0), (0, 1)), (R32::new(1.0), (1, 1)), (R32::new(-0.125), (2, 1,)),
            (R32::new(-1.0), (4, 3)), (R32::new(1.5), (0, 0)),
        ]);
        calc.vecmul_delta(
            &del,
            &srcbuffer, &destbuffer, SpatialDims::Two(hsize, vsize), 
            locsize, vecsize
        ).unwrap();
        manucl::read_buffer(&destbuffer, &mut datadest).unwrap();

        let prec = DEFAULT_PREC;
        for ipre in 1..15 {
            for jpre in 2..15 {
                let i = ipre*hsize/16;
                let j = jpre*vsize/16;
                assert_float_relative_eq_m!(
                    datadest[[i, j]],
                    1.5f32*data[[i-2, j-17]] - 0.25f32*data[[i-2, j-16]]
                    - 3.0f32*data[[i-1, j-1]] + 2.0f32*data[[i-1, j]]
                    + 1.5f32*data[[i-1, j+1]]
                    - 1.0f32*data[[i, j+1]] + 1.0f32*data[[i+1, j+1]]
                    - 0.125f32*data[[i+2, j+1]]
                    - 1.0f32*data[[i+4, j+3]] + 1.5f32*data[[i, j]],
                    prec,
                    "Wrong result at ({}, {}) (regular case behaviour)", i, j
                );
            }
        }

        for ipre in 1..15 {
            for jpre in 1..15 {
                let i = ipre*hsize/16 + 5;
                let j = jpre*vsize/16 + 3;
                assert_float_relative_eq_m!(
                    datadest[[i, j]],
                    1.5f32*data[[i-2, j-17]] - 0.25f32*data[[i-2, j-16]]
                    - 3.0f32*data[[i-1, j-1]] + 2.0f32*data[[i-1, j]]
                    + 1.5f32*data[[i-1, j+1]]
                    - 1.0f32*data[[i, j+1]] + 1.0f32*data[[i+1, j+1]]
                    - 0.125f32*data[[i+2, j+1]]
                    - 1.0f32*data[[i+4, j+3]] + 1.5f32*data[[i, j]],
                    prec,
                    "Wrong result at ({}, {}) (odd case behaviour)", i, j
                );
            }
        }

        for ipre in 1..15 {
            let i = ipre*hsize/16+2;
            let mut j = 0;
            assert_float_relative_eq_m!(
                datadest[[i, j]],
                2.0f32*data[[i-1, j]]
                + 1.5f32*data[[i-1, j+1]]
                - 1.0f32*data[[i, j+1]] + 1.0f32*data[[i+1, j+1]]
                - 0.125f32*data[[i+2, j+1]]
                - 1.0f32*data[[i+4, j+3]] + 1.5f32*data[[i, j]],
                prec,
                "Wrong result at ({}, {}) (edge case behaviour)", i, j
            );
            j = 9;
            assert_float_relative_eq_m!(
                datadest[[i, j]],
                - 3.0f32*data[[i-1, j-1]] + 2.0f32*data[[i-1, j]]
                + 1.5f32*data[[i-1, j+1]]
                - 1.0f32*data[[i, j+1]] + 1.0f32*data[[i+1, j+1]]
                - 0.125f32*data[[i+2, j+1]]
                - 1.0f32*data[[i+4, j+3]] + 1.5f32*data[[i, j]],
                prec,
                "Wrong result at ({}, {}) (edge case behaviour)", i, j
            );
            j = vsize-1;
            assert_float_relative_eq_m!(
                datadest[[i, j]],
                1.5f32*data[[i-2, j-17]] - 0.25f32*data[[i-2, j-16]]
                - 3.0f32*data[[i-1, j-1]] + 2.0f32*data[[i-1, j]]
                 + 1.5f32*data[[i, j]],
                prec,
                "Wrong result at ({}, {}) (edge case behaviour)", i, j
            );
            j = vsize-3;
            assert_float_relative_eq_m!(
                datadest[[i, j]],
                1.5f32*data[[i-2, j-17]] - 0.25f32*data[[i-2, j-16]]
                - 3.0f32*data[[i-1, j-1]] + 2.0f32*data[[i-1, j]]
                + 1.5f32*data[[i-1, j+1]]
                - 1.0f32*data[[i, j+1]] + 1.0f32*data[[i+1, j+1]]
                - 0.125f32*data[[i+2, j+1]] + 1.5f32*data[[i, j]],
                prec,
                "Wrong result at ({}, {}) (edge case behaviour)", i, j
            );
        }

        for jpre in 1..14 {
            let mut j = jpre*vsize/16 + 6;
            let mut i = 0;
                assert_float_relative_eq_m!(
                    datadest[[i, j]],
                    - 1.0f32*data[[i, j+1]] + 1.0f32*data[[i+1, j+1]]
                    - 0.125f32*data[[i+2, j+1]]
                    - 1.0f32*data[[i+4, j+3]] + 1.5f32*data[[i, j]],
                    prec,
                    "Wrong result at ({}, {}) (edge case behaviour)", i, j
                );
            j = jpre*vsize/16 + 8;
            i = 1;
                assert_float_relative_eq_m!(
                    datadest[[i, j]],
                    - 3.0f32*data[[i-1, j-1]] + 2.0f32*data[[i-1, j]]
                    + 1.5f32*data[[i-1, j+1]]
                    - 1.0f32*data[[i, j+1]] + 1.0f32*data[[i+1, j+1]]
                    - 0.125f32*data[[i+2, j+1]]
                    - 1.0f32*data[[i+4, j+3]] + 1.5f32*data[[i, j]],
                    prec,
                    "Wrong result at ({}, {}) (edge case behaviour)", i, j
                );
            i = hsize-2;
                assert_float_relative_eq_m!(
                    datadest[[i, j]],
                    1.5f32*data[[i-2, j-17]] - 0.25f32*data[[i-2, j-16]]
                    - 3.0f32*data[[i-1, j-1]] + 2.0f32*data[[i-1, j]]
                    + 1.5f32*data[[i-1, j+1]]
                    - 1.0f32*data[[i, j+1]] + 1.0f32*data[[i+1, j+1]]
                     + 1.5f32*data[[i, j]],
                    prec,
                    "Wrong result at ({}, {}) (edge case behaviour)", i, j
                );
        }

        let mut i = 0;
        let mut j = 0;
        assert_float_relative_eq_m!(
            datadest[[i, j]],
            - 1.0f32*data[[i, j+1]] + 1.0f32*data[[i+1, j+1]]
            - 0.125f32*data[[i+2, j+1]]
            - 1.0f32*data[[i+4, j+3]] + 1.5f32*data[[i, j]],
            prec,
            "Wrong result at ({}, {}) (corner case behaviour)", i, j
        );
        i = hsize-1;
        j = 0;
        assert_float_relative_eq_m!(
            datadest[[i, j]],
            2.0f32*data[[i-1, j]]
            + 1.5f32*data[[i-1, j+1]]
            - 1.0f32*data[[i, j+1]] + 1.5f32*data[[i, j]],
            prec,
            "Wrong result at ({}, {}) (corner case behaviour)", i, j
        );
        i = 0;
        j = vsize-1;
        assert_float_relative_eq_m!(
            datadest[[i, j]],
            1.5f32*data[[i, j]],
            prec,
            "Wrong result at ({}, {}) (corner case behaviour)", i, j
        );
        i = hsize-1;
        j = vsize-1;
        assert_float_relative_eq_m!(
            datadest[[i, j]],
            1.5f32*data[[i-2, j-17]] - 0.25f32*data[[i-2, j-16]]
            - 3.0f32*data[[i-1, j-1]] + 2.0f32*data[[i-1, j]]
            + 1.5f32*data[[i, j]],
            prec,
            "Wrong result at ({}, {}) (corner case behaviour)", i, j
        );
        i = hsize-3;
        j = vsize-9;
        assert_float_relative_eq_m!(
            datadest[[i, j]],
            1.5f32*data[[i-2, j-17]] - 0.25f32*data[[i-2, j-16]]
            - 3.0f32*data[[i-1, j-1]] + 2.0f32*data[[i-1, j]]
            + 1.5f32*data[[i-1, j+1]]
            - 1.0f32*data[[i, j+1]] + 1.0f32*data[[i+1, j+1]]
            - 0.125f32*data[[i+2, j+1]] + 1.5f32*data[[i, j]],
            prec,
            "Wrong result at ({}, {}) (corner case behaviour)", i, j
        );
    }

    #[test]
    // Ignored because it fails if not run with --test-threads 1
    #[ignore]
    pub fn vecmul_diag_two_dim() {
        let openclini = OpenCLInitialization::from_info(ocl::flags::DeviceType::CPU, "", "")
            .expect("Could not create OpenCLInitialization");
        let hsize = 512usize;
        let vsize = 512usize;
        let locsize = default_locsize(&openclini, 2);
        let vecsize = DEFAULT_VECSIZE;
        let mut data: Array2<f32>;
        let mut datadest: Array2<f32>;
        let mut diag: [Array2<f32>; 2];
        unsafe {
            data = Array2::uninitialized((hsize, vsize));
            datadest = Array2::uninitialized((hsize, vsize));
            diag = [Array2::uninitialized((hsize, vsize)), Array2::uninitialized((hsize, vsize))];
        }
        let between = distributions::Uniform::from(0..10000);
        let mut rng = rand::thread_rng();
        for a in diag[0].iter_mut() {
            *a = between.sample(&mut rng) as f32;
        }
        for a in diag[1].iter_mut() {
            *a = between.sample(&mut rng) as f32;
        }
        let srcbuffer = manucl::buffer_from_ndarray(&data, &openclini).unwrap();
        let destbuffer = manucl::buffer_from_ndarray(&datadest, &openclini).unwrap();
        let (diagbuffer0, diagbuffer1) = (
            manucl::buffer_from_ndarray(&diag[0], &openclini).unwrap(),
            manucl::buffer_from_ndarray(&diag[1], &openclini).unwrap(),
        );
        fill_two_d(&openclini, &mut data, &srcbuffer, (hsize, vsize), locsize);

        let mut calc = CLProgramCollector::<f32>::new(openclini);
        
        let del = DeltaMat::Two(vec![
            (R32::new(1.5), (0, 0)), (R32::new(-0.75), (-1, 0)),
        ]);
        let di = DiagMat::Two(
            vec![(diagbuffer0, (0, 0)), (diagbuffer1, (0, 1))]
        );
        calc.vecmul_diag(
            &di,
            Some(del),
            &srcbuffer, &destbuffer, SpatialDims::Two(hsize, vsize), 
            locsize, vecsize
        ).unwrap();
        manucl::read_buffer(&destbuffer, &mut datadest).unwrap();

        let prec = DEFAULT_PREC;
        for ipre in 1..15 {
            for jpre in 2..15 {
                let i = ipre*hsize/16;
                let j = jpre*vsize/16;
                assert_float_relative_eq_m!(
                    datadest[[i, j]],
                    1.5*data[[i, j]] - 0.75*data[[i-1, j]]
                    + diag[0][[i, j]]*data[[i, j]] + diag[1][[i, j]]*data[[i, j+1]],
                    prec,
                    "Wrong result at ({}, {}) (regular case behaviour)", i, j
                );
            }
        }

        for ipre in 1..15 {
            for jpre in 1..15 {
                let i = ipre*hsize/16 + 5;
                let j = jpre*vsize/16 + 3;
                assert_float_relative_eq_m!(
                    datadest[[i, j]],
                    1.5*data[[i, j]] - 0.75*data[[i-1, j]]
                    + diag[0][[i, j]]*data[[i, j]] + diag[1][[i, j]]*data[[i, j+1]],
                    prec,
                    "Wrong result at ({}, {}) (odd case behaviour)", i, j
                );
            }
        }

        for ipre in 1..15 {
            let i = ipre*hsize/16+2;
            let mut j = 0;
            assert_float_relative_eq_m!(
                datadest[[i, j]],
                1.5*data[[i, j]] - 0.75*data[[i-1, j]]
                + diag[0][[i, j]]*data[[i, j]] + diag[1][[i, j]]*data[[i, j+1]],
                prec,
                "Wrong result at ({}, {}) (edge case behaviour)", i, j
            );
            j = 9;
            assert_float_relative_eq_m!(
                datadest[[i, j]],
                1.5*data[[i, j]] - 0.75*data[[i-1, j]]
                + diag[0][[i, j]]*data[[i, j]] + diag[1][[i, j]]*data[[i, j+1]],
                prec,
                "Wrong result at ({}, {}) (edge case behaviour)", i, j
            );
            j = vsize-1;
            assert_float_relative_eq_m!(
                datadest[[i, j]],
                1.5*data[[i, j]] - 0.75*data[[i-1, j]]
                + diag[0][[i, j]]*data[[i, j]],
                prec,
                "Wrong result at ({}, {}) (edge case behaviour)", i, j
            );
            j = vsize-3;
            assert_float_relative_eq_m!(
                datadest[[i, j]],
                1.5*data[[i, j]] - 0.75*data[[i-1, j]]
                + diag[0][[i, j]]*data[[i, j]] + diag[1][[i, j]]*data[[i, j+1]],
                prec,
                "Wrong result at ({}, {}) (edge case behaviour)", i, j
            );
        }

        for jpre in 1..14 {
            let mut j = jpre*vsize/16 + 6;
            let mut i = 0;
            assert_float_relative_eq_m!(
                datadest[[i, j]],
                1.5*data[[i, j]]
                + diag[0][[i, j]]*data[[i, j]] + diag[1][[i, j]]*data[[i, j+1]],
                prec,
                "Wrong result at ({}, {}) (edge case behaviour)", i, j
            );
            j = jpre*vsize/16 + 8;
            i = 1;
            assert_float_relative_eq_m!(
                datadest[[i, j]],
                1.5*data[[i, j]] - 0.75*data[[i-1, j]]
                + diag[0][[i, j]]*data[[i, j]] + diag[1][[i, j]]*data[[i, j+1]],
                prec,
                "Wrong result at ({}, {}) (edge case behaviour)", i, j
            );
            i = hsize-2;
            assert_float_relative_eq_m!(
                datadest[[i, j]],
                1.5*data[[i, j]] - 0.75*data[[i-1, j]]
                + diag[0][[i, j]]*data[[i, j]] + diag[1][[i, j]]*data[[i, j+1]],
                prec,
                "Wrong result at ({}, {}) (edge case behaviour)", i, j
            );
        }

        let mut i = 0;
        let mut j = 0;
        assert_float_relative_eq_m!(
            datadest[[i, j]],
            1.5*data[[i, j]]
            + diag[0][[i, j]]*data[[i, j]] + diag[1][[i, j]]*data[[i, j+1]],
            prec,
            "Wrong result at ({}, {}) (corner case behaviour)", i, j
        );
        i = hsize-1;
        j = 0;
        assert_float_relative_eq_m!(
            datadest[[i, j]],
            1.5*data[[i, j]] - 0.75*data[[i-1, j]]
            + diag[0][[i, j]]*data[[i, j]] + diag[1][[i, j]]*data[[i, j+1]],
            prec,
            "Wrong result at ({}, {}) (corner case behaviour)", i, j
        );
        i = 0;
        j = vsize-1;
        assert_float_relative_eq_m!(
            datadest[[i, j]],
            1.5*data[[i, j]]
            + diag[0][[i, j]]*data[[i, j]],
            prec,
            "Wrong result at ({}, {}) (corner case behaviour)", i, j
        );
        i = hsize-1;
        j = vsize-1;
        assert_float_relative_eq_m!(
            datadest[[i, j]],
            1.5*data[[i, j]] - 0.75*data[[i-1, j]]
            + diag[0][[i, j]]*data[[i, j]],
            prec,
            "Wrong result at ({}, {}) (corner case behaviour)", i, j
        );
        i = hsize-3;
        j = vsize-9;
        assert_float_relative_eq_m!(
            datadest[[i, j]],
            1.5*data[[i, j]] - 0.75*data[[i-1, j]]
            + diag[0][[i, j]]*data[[i, j]] + diag[1][[i, j]]*data[[i, j+1]],
            prec,
            "Wrong result at ({}, {}) (corner case behaviour)", i, j
        );
    }

    #[test]
    // Ignored because it fails if not run with --test-threads 1
    #[ignore]
    pub fn add_to() {
        let openclini = default_openclini()
            .expect("Could not create OpenCLInitialization");
        let hsize = 1024usize;
        let vsize = 1024usize;
        let locsize = default_locsize(&openclini, 2);
        let vecsize = DEFAULT_VECSIZE;
        let mut left: Array2<f32>;
        let mut right: Array2<f32>;
        let mut dest: Array2<f32>;
        unsafe {
            left = Array2::uninitialized((hsize, vsize));
            right = Array2::uninitialized((hsize, vsize));
            dest = Array2::uninitialized((hsize, vsize));
        }
        let between = distributions::Uniform::from(5000..10000);
        let mut rng = rand::thread_rng();
        for i in 0..hsize {
            for j in 0..vsize {
                left[[i, j]] = between.sample(&mut rng) as f32;
                right[[i, j]] = between.sample(&mut rng) as f32;
            }
        }
        let srcbuffer = manucl::buffer_from_ndarray(&left, &openclini).unwrap();
        let destbuffer = manucl::buffer_from_ndarray(&right, &openclini).unwrap();

        let mut calc = CLProgramCollector::<f32>::new(openclini);
        
        calc.add_to(
            vecsize, SpatialDims::Two(hsize, vsize), locsize, 
            &srcbuffer, &destbuffer
        ).unwrap();
        manucl::read_buffer(&destbuffer, &mut dest).unwrap();

        let prec = DEFAULT_PREC;
        for ipre in 1..15 {
            for jpre in 2..15 {
                let i = ipre*hsize/16;
                let j = jpre*vsize/16;
                assert_float_relative_eq_m!(
                    dest[[i, j]],
                    left[[i, j]] + right[[i, j]],
                    prec,
                    "Wrong result at ({}, {}) (regular case behaviour)", i, j
                );
            }
        }

        for ipre in 1..15 {
            for jpre in 1..15 {
                let i = ipre*hsize/16 + 5;
                let j = jpre*vsize/16 + 3;
                assert_float_relative_eq_m!(
                    dest[[i, j]],
                    left[[i, j]] + right[[i, j]],
                    prec,
                    "Wrong result at ({}, {}) (odd case behaviour)", i, j
                );
            }
        }
    }

    #[test]
    // Ignored because it fails if not run with --test-threads 1
    #[ignore]
    pub fn scalarmul_to() {
        let openclini = default_openclini()
            .expect("Could not create OpenCLInitialization");
        let hsize = 1024usize;
        let vsize = 1024usize;
        let locsize = default_locsize(&openclini, 2);
        let vecsize = DEFAULT_VECSIZE;
        let mut right: Array2<f32>;
        let mut dest: Array2<f32>;
        let factor = 5f32;
        unsafe {
            right = Array2::uninitialized((hsize, vsize));
            dest = Array2::uninitialized((hsize, vsize));
        }
        let between = distributions::Uniform::from(5000..10000);
        let mut rng = rand::thread_rng();
        for i in 0..hsize {
            for j in 0..vsize {
                right[[i, j]] = between.sample(&mut rng) as f32;
            }
        }
        let destbuffer = manucl::buffer_from_ndarray(&right, &openclini).unwrap();

        let mut calc = CLProgramCollector::<f32>::new(openclini);
        
        calc.scalarmul_to(
            vecsize, SpatialDims::Two(hsize, vsize), locsize, 
            factor, &destbuffer
        ).unwrap();
        manucl::read_buffer(&destbuffer, &mut dest).unwrap();

        let prec = DEFAULT_PREC;
        for ipre in 1..15 {
            for jpre in 2..15 {
                let i = ipre*hsize/16;
                let j = jpre*vsize/16;
                assert_float_relative_eq_m!(
                    dest[[i, j]],
                    factor*right[[i, j]],
                    prec,
                    "Wrong result at ({}, {}) (regular case behaviour)", i, j
                );
            }
        }

        for ipre in 1..15 {
            for jpre in 1..15 {
                let i = ipre*hsize/16 + 5;
                let j = jpre*vsize/16 + 3;
                assert_float_relative_eq_m!(
                    dest[[i, j]],
                    factor*right[[i, j]],
                    prec,
                    "Wrong result at ({}, {}) (odd case behaviour)", i, j
                );
            }
        }
    }

    #[test]
    // Ignored because it fails if not run with --test-threads 1
    #[ignore]
    pub fn scalar_prod() {
        let openclini = default_openclini()
            .expect("Could not create OpenCLInitialization");
        let hsize = 4096usize;
        let locsize = (16usize, 16usize);
        let vecsize = (8u8, 2u8);
        let mut left: Array1<f32>;
        let mut right: Array1<f32>;
        unsafe {
            left = Array1::uninitialized((hsize,));
            right = Array1::uninitialized((hsize,));
        }
        let between = distributions::Uniform::from(5000..10000);
        let mut rng = rand::thread_rng();
        for i in 0..hsize {
            left[[i]] = between.sample(&mut rng) as f32;
            right[[i]] = between.sample(&mut rng) as f32;
        }
        let leftbuffer = manucl::buffer_from_ndarray(&left, &openclini).unwrap();
        let rightbuffer = manucl::buffer_from_ndarray(&right, &openclini).unwrap();

        let mut calc = CLProgramCollector::<f32>::new(openclini);
        
        let res = calc.scalar_prod(
            &leftbuffer, &rightbuffer, 
            locsize, (8, 2), vecsize,
        ).unwrap();

        let prec = DEFAULT_PREC;
        assert_float_relative_eq_m!(
            res, left.dot(&right),
            prec,
            "Wrong result of scalar product"
        );
    }

    #[test]
    // Ignored because it fails if not run with --test-threads 1
    #[ignore]
    fn linear_solve_dense() {
        let openclini = default_openclini()
            .expect("Could not create OpenCLInitialization");
        let locsize = default_locsize(&openclini, 2);
        let hsize = 64usize;
        let locsize = if let SpatialDims::Two(h, v) = locsize {
            (h, v)
        } else {
            panic!();
        };
        //let locsize = (hsize, vsize);
        let vecsize = DEFAULT_VECSIZE;
        
        let mut a: Array2<f32> = Array2::zeros((hsize, hsize));
        let mut b: Array1<f32> = Array1::ones(hsize);
        let mut dest: Array1<f32> = Array1::zeros(hsize);
        let between = distributions::Uniform::from(8000..10000);
        let mut rng = rand::thread_rng();
        for i in 0..hsize {
            b[i] = between.sample(&mut rng) as f32;
        }
        for i in 0..hsize {
            for j in 0..hsize {
                a[[i, j]] = 1.0f32/(1.0f32 + (i as f32 - j as f32).powi(2));
                //a[[i, j]] = between.sample(&mut rng) as f32;
                //a[[i, j]] = if i == j {2.0f32} else {0.0f32};
                //a[[i, j]] = if i == j {2.0f32} else if i == j+1 {0.0f32} else if i+1 == j {1.0f32} else {0.0f32};
            }
        }
        let abuffer = manucl::buffer_from_ndarray(&a, &openclini).unwrap();
        let bbuffer = manucl::buffer_from_ndarray(&b, &openclini).unwrap();
        let destbuffer = manucl::buffer_from_ndarray(&dest, &openclini).unwrap();

        let mut calc = CLProgramCollector::<f32>::new(openclini);

        calc.linear_solve_dense(
            &abuffer, &bbuffer, &destbuffer, 
            locsize, vecsize
        ).unwrap();
        manucl::read_buffer(&destbuffer, &mut dest).unwrap();

        let prec = 1./(0x400 as f32);
        let bguess = a.dot(&dest);
        for i in 0..hsize {
            assert_float_relative_eq_m!(
                b[i], bguess[i], prec,
                "Wrong result at {}", i
            );
        }
    }
}