/* simulation_qm
 * Copyright 2018 Manuel Simon
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 * */

/*!
 * This module contains helper functions and structs for
 * working with OpenCL.
 * 
 * It is based on the ocl package from Nick Sanders.
 * 
 * There is currently the struct [`OpenCLInitialization`](manucl::OpenCLInitialization),
 * which collects several OpenCL related structs and provides methods
 * to initialize them, and some helper functions around [`ocl::SpatialDims`].
 */

use ocl;
use ocl::{Platform, Device, Context, Queue, Buffer};
use ocl::{SpatialDims, flags::DeviceType};
use ocl::enums::{DeviceInfo, DeviceInfoResult};
use rand::random;
use ndarray::ArrayBase;


use super::manuconvenience;


/// Collection of a standard context, device and queue for OpenCL, together with some functions
/// for choosing devices (like 
/// [`from_fastest_device`](OpenCLInitialization::from_fastest_device) or recommendations
/// for kernel calls 
/// (like [`max_work_group_dimensions`](OpenCLInitialization::max_work_group_dimensions)).
#[derive(Clone)]
pub struct OpenCLInitialization {
    pub id: (u32, String),
    pub stdcontext: Context,
    pub stddevice: Device,
    pub stdqueue: Queue,
}

impl OpenCLInitialization {
    /// Creates an `OpenCLInitialization` with standard device `dev`
    /// and platform `pl`.
    ///
    /// # Errors
    /// 
    /// When creating of context or queue fails.
    pub fn from_device_and_platform(dev: Device, pl: Platform)
            -> Result<OpenCLInitialization, ocl::error::Error> {
        let stddevice = dev;
        let stdcontext = Context::builder()
            .platform(pl)
            .devices(stddevice)
            .build()?;
        let stdqueue = Queue::new(&stdcontext, stddevice, None)?;
        let devid = if let DeviceInfoResult::VendorId(x) = stddevice.info(DeviceInfo::VendorId)
            .unwrap_or(DeviceInfoResult::VendorId(random::<u32>())) {
                x
            } else {
                random::<u32>()
            };
        let devplat = if let DeviceInfoResult::Vendor(x) = stddevice.info(DeviceInfo::Vendor)
            .unwrap_or(DeviceInfoResult::Vendor(random::<u16>().to_string())) {
                x
            } else {
                random::<u16>().to_string()
            };
        Ok(OpenCLInitialization {
            id: (devid, devplat),
            stddevice,
            stdcontext,
            stdqueue,
        })
    }
    pub fn from_info(
            devtype: DeviceType,
            vendor: &str, name: &str)
            -> Result<OpenCLInitialization, ocl::error::Error> {
        if let Some(d) = select_fastest_device_with_info(devtype, vendor, name) {
            OpenCLInitialization::from_device_and_platform(d.0, d.1)
        } else {
            Err(ocl::error::Error::from("No device with these properties available"))
        }
    }

    /// Creates an `OpenCLInitialization` with the device returned from [`select_fastest_device`]
    /// as standard device and [`stdcontext`](OpenCLInitialization::stdcontext) and 
    /// [`stdqueue`](OpenCLInitialization::stdqueue) created from this device
    /// and its platform.
    ///
    /// # Errors
    /// When there are no usable OpenCL-devices
    pub fn from_fastest_device() -> Result<OpenCLInitialization, ocl::error::Error> {
        if let Some(d) = select_fastest_device() {
            OpenCLInitialization::from_device_and_platform(d.0, d.1)
        } else {
            Err(ocl::error::Error::from("No usable device available"))
        }
    }

    /// Returs the maximal choosable work group dimensions in each dimension for a
    /// n-dimensional kernel call that are a power of two.
    ///
    /// # Errors
    /// When dimension is not 1 or 2.
    pub fn max_work_group_dimensions(& self, dim: u16) -> Result<SpatialDims, ocl::error::Error> {
        let max_dimensions = self.stddevice.info(DeviceInfo::MaxWorkGroupSize)?;
        if dim == 1 {
            if let DeviceInfoResult::MaxWorkGroupSize(m) = max_dimensions {
                Ok(SpatialDims::One(manuconvenience::round_to_floor_p2(m)))
            } else {
                Err(ocl::error::Error::from("Unknown error!"))
            }
        } else if dim == 2 {
            if let DeviceInfoResult::MaxWorkGroupSize(m) = max_dimensions {
                let single_dimenions: usize
                        = manuconvenience::round_to_floor_p2((m as f32).sqrt()) as usize;
                if single_dimenions*2 < m {
                    Ok(SpatialDims::Two(2*single_dimenions, single_dimenions))
                } else {
                    Ok(SpatialDims::Two(single_dimenions, single_dimenions))
                }
            } else {
                Err(ocl::error::Error::from("Unknown error!"))
            }
        } else {
            Err(ocl::error::Error::from("Can currently only process dimensions one and two"))
        }
    }
}


/// Returns the fastest OpenCL Device of type devtype, whose vendor name
/// contains vendor and whose name contains name.
/// Performance based on the same rudimentary calculation used in [`select_fastest_device`].
pub fn select_fastest_device_with_info(
        devtype: DeviceType,
        vendor: &str, name: &str)
        -> Option<(Device, Platform)> {
    let platform = Platform::list();
    let mut dev: Vec<(Device, &Platform)> = Vec::new();

    for pl in platform.iter() {
        for d in Device::list(pl, Some(devtype))
            .unwrap_or(Vec::new()) {
            if d.vendor().unwrap_or(String::new()).to_lowercase()
                        .contains(vendor.to_lowercase().as_str())
                    && d.name().unwrap_or(String::new()).to_lowercase()
                        .contains(name.to_lowercase().as_str()) {
                dev.push((d, pl));
            }
        }
    }
    let mut stddevice: Option<(Device, &Platform)> = None;
    let mut maxperformance = 0;
    select_best_performance(dev, &mut stddevice, &mut maxperformance);
    if let Some(res) = stddevice {
        Some((res.0, res.1.clone()))
    } else {
        None
    }
}
/// Returns the fastest OpenCL-device based on a rudimentary calculation off
/// MaxComputeUnits*MaxClockFrequency and its kind.
/// OpenCL accelerators are preferred over GPUs, are preferred over CPUs.
pub fn select_fastest_device() -> Option<(Device, Platform)> {
    /* We first sort the devices on all platforms into three vecs for the
     * three kinds accelerator, GPU and CPU. In the first of this list
     * which contains a usable device we choosed the fastest, based on
     * the rudimentary calculation above.
     **/
    let platform = Platform::list();
    let mut acc: Vec<(Device, &Platform)> = Vec::new();
    let mut gpu: Vec<(Device, &Platform)> = Vec::new();
    let mut cpu: Vec<(Device, &Platform)> = Vec::new();

    for pl in platform.iter() {
        for d in Device::list(pl, Some(ocl::flags::DeviceType::ACCELERATOR))
            .unwrap_or(Vec::new()) {
            acc.push((d, pl));
        }
        for d in Device::list(pl, Some(ocl::flags::DeviceType::GPU))
            .unwrap_or(Vec::new()) {
            gpu.push((d, pl));
        }
        for d in Device::list(pl, Some(ocl::flags::DeviceType::CPU))
            .unwrap_or(Vec::new()) {
            cpu.push((d, pl));
        }
    }

    // stddevice stays None until any usable device is found.
    let mut stddevice: Option<(Device, &Platform)> = None;
    let mut maxperformance = 0;
    select_best_performance(acc, &mut stddevice, &mut maxperformance);
    if let Some(res) = stddevice {
        return Some((res.0, res.1.clone()));
    }
    select_best_performance(gpu, &mut stddevice, &mut maxperformance);
    if let Some(res) = stddevice {
        return Some((res.0, res.1.clone()));
    }
    select_best_performance(cpu, &mut stddevice, &mut maxperformance);
    if let Some(res) = stddevice {
        Some((res.0, res.1.clone()))
    } else {
        None
    }
}


/// Searches dev for the devices with a performance
/// that is better than maxperformance, based on the rudimentary calculation
/// MaxComputeUnits*MaxClockFrequency. If one is found, 
/// [`OpenCLInitialization::stddevice`] is set to
/// the device with best performance and maxperformance to its performance.
/// If not, nothing is done.
fn select_best_performance<'a>(
        mut dev: Vec<(Device, &'a Platform)>,
        stddevice: &mut Option<(Device, &'a Platform)>,
        maxperformance: &mut u32) {
    loop {
        if let Some(d) = dev.pop() {
            if let DeviceInfoResult::MaxComputeUnits(cunits) =
                    d.0.info(DeviceInfo::MaxComputeUnits)
                    .unwrap_or(DeviceInfoResult::MaxComputeUnits(0)) {
                if let DeviceInfoResult::MaxClockFrequency(clock_freq) =
                        d.0.info(DeviceInfo::MaxClockFrequency)
                        .unwrap_or(DeviceInfoResult::MaxClockFrequency(0)) {
                    if cunits*clock_freq > *maxperformance {
                        *maxperformance = cunits*clock_freq;
                        *stddevice = Some(d);
                    }
                }
            }
        } else {
            break;
        }
    }
}


/**
 * Returns the last component of a [`ocl::SpatialDims`]. 
 * 
 * 0 is returned if it is `Unspecified`.
 */
pub fn last_spatial_dim(u: SpatialDims) -> usize {
    match u {
        SpatialDims::One(u1) => u1,
        SpatialDims::Two(_u1, u2) => u2,
        SpatialDims::Three(_u1, _u2, u3) => u3,
        SpatialDims::Unspecified => 0,
    }
}

/**
 * Returns the total size of a [`ocl::SpatialDims`]. This is the product of 
 * all components.
 */
pub fn total_spatial_dims(u: SpatialDims) -> usize {
    match u {
        SpatialDims::One(u1) => u1,
        SpatialDims::Two(u1, u2) => u1*u2,
        SpatialDims::Three(u1, u2, u3) => u1*u2*u3,
        SpatialDims::Unspecified => 0,
    }
}

/**
 * # Panics
 * 
 * If the array does not have standard layout.
 */
pub fn buffer_from_ndarray<E: ocl::OclPrm, S: ndarray::Data<Elem=E>, D: ndarray::Dimension>(
    array: & ArrayBase<S, D>,
    openclini: & OpenCLInitialization,
) -> Result<Buffer<E>, ocl::Error> {
    let shape = array.shape();
    let dims = match shape.len() {
        0 => panic!("Array must not be zero-dimensional"),
        1 => SpatialDims::One(shape[0]),
        2 => SpatialDims::Two(shape[0], shape[1]),
        3 => SpatialDims::Three(shape[0], shape[1], shape[2]),
        n => {
            let mut res = 1usize;
            for i in 2..n {
                res *= shape[i];
            }
            SpatialDims::Three(shape[0], shape[1], res)
        }
    };
    let res: Buffer<E> = Buffer::builder()
        .len(dims)
        .queue(openclini.stdqueue.clone())
        .build()?;

    match array.as_slice() {
        Some(s) => res.write(s).enq().unwrap(),
        //DANGER OF PANIC
        None => panic!("Array has non standard layout"),
    }
    Ok(res)
}

/**
 * # Panics
 * 
 * If the array does not have standard layout.
 */
pub fn read_buffer<E: ocl::OclPrm, S: ndarray::DataMut<Elem=E>, D: ndarray::Dimension>(
    buf: & Buffer<E>,
    array: &mut ArrayBase<S, D>,
) -> Result<(), ocl::Error> {
    match array.as_slice_mut() {
        Some(s) => buf.read(s).enq(),
        //DANGER OF PANIC
        None => panic!("Array has non standard layout"),
    }
}


#[cfg(test)]
mod tests {
    use super::*;
    use ocl::flags::DeviceType;

    use ndarray::Array1;

    use rand::distributions;
    use rand::distributions::Distribution;

    use crate::tests::default_openclini;

    #[test]
    // Ignored because it fails if not run with --test-threads 1
    #[ignore]
    fn select_device_t() {
        let o = OpenCLInitialization::from_fastest_device().unwrap();
        println!("Fastest:\n{}", o.stddevice.to_string());
        let o = OpenCLInitialization::from_info(
            DeviceType::GPU,
            "Nvidia", "").unwrap();
        println!("Fastest from Nvidia:\n{}", o.stddevice.to_string());
    }

    #[test]
    // Ignored because it fails if not run with --test-threads 1
    #[ignore]
    fn list_devices() {
        let platform = Platform::list();
        for pl in platform.iter() {
            println!("Platform {} ({}):\n",
                pl.name().unwrap_or("unknown".to_string()),
                pl.vendor().unwrap_or("unknown".to_string()));
            for dev in Device::list(pl, None).unwrap() {
                if let DeviceInfoResult::MaxComputeUnits(cunits) =
                    dev.info(DeviceInfo::MaxComputeUnits)
                    .unwrap_or(DeviceInfoResult::MaxComputeUnits(0)) {
                if let DeviceInfoResult::MaxClockFrequency(clock_freq) =
                        dev.info(DeviceInfo::MaxClockFrequency)
                        .unwrap_or(DeviceInfoResult::MaxClockFrequency(0)) {
                        println!("{} ({}):  {}⋅{} Mhz\n",
                            dev.name().unwrap_or("unknown".to_string()),
                            dev.vendor().unwrap_or("unknown".to_string()),
                            cunits,
                            clock_freq);
                    }
                }
            }
        }
    println!("\n\n");
    }

    
    #[test]
    // Ignored because it fails if not run with --test-threads 1
    #[ignore]
    fn read_write_buffer() {
        let mut a: Array1<u16> = Array1::zeros(0x80);

        let between = distributions::Uniform::from(0..4096);
        let mut rng = rand::thread_rng();
        for v in a.iter_mut() {
            *v = between.sample(&mut rng);
        }
        let openclini = default_openclini().unwrap();
        let buf = buffer_from_ndarray(&a, &openclini).unwrap();
        let mut b: Array1<u16> = Array1::zeros(a.len());
        read_buffer(&buf, &mut b).unwrap();
        for (i, (v, w)) in Iterator::enumerate(Iterator::zip(a.iter(), b.iter())) {
            assert_eq!(
                v, w,
                "Wrong value at position {}", i
            );
        }
    }
}