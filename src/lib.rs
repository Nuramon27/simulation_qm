/* simulation_qm
 * Copyright 2018 Manuel Simon
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 * */


/*!
 * This module currently provides functionality for doing linear algebraic
 * operations with OpenCL. It provides functions for applying all powers, 
 * including negative ones, of a matrix to large vectors.
 * 
 * It is especially build around operations with so-called delta matrices,
 * a simple verson of sparse matrices.
 * 
 * # About Delta Matrices.
 * These are matrices whose components can be written
 * as a linear combination of Kronecker Deltas, possibly multiplied by a further function of the
 * index. The indices on the Kronecker Delta is considered to have the form
 * d_{i+del, j} (so there may not be a factor before the i or the j).
 * E.g. the discrete derivative is a delta matrix as it can be written as
 * d_{i+1, j} - d_{i-1, j}. If you write the components of a vector A_i at the
 * diagonal entries of a matrix, you get a delta matrix, too: A_i d_{i, j},
 * where the Kronecker delta is multiplied with a function of the index.
 *
 * The specialty of the functions in this file is that they can not only
 * handle matrices that describe linear maps of one-dimensional vectors,
 * but also of tensor of second or third (...) stage, which is necessary
 * to solve PDEs in more than one dimension by discretization. Those higher
 * dimensional matrices have then, if dim is the dimension, 2*dim indices
 * and for example the product of a "three-dimensional" matrix M_{ijklmn} with a three
 * dimensional vector v_{jln} is then M_{ijklmn}*v_{jln} so you sum up over
 * every second index of the matrix. The higher dimensional delta matrixes
 * are then a linear combination of products of dim Kronecker-deltas.
 * E.g the Laplacian in two dimensions is then (in its simplest form)
 * 
 * ```C
 * L_{ijkl} = d_{i+1,j}d_{k,l} + d_{i-1,j}d_{k,l}
 * - 4d_{i,j}d_{k,l} + d{i,j}d_{k+1,l} + d{i,j}d_{k-1,1}
 * ```
 * 
 * Structs for representing delta matrices are [`DeltaMat`] and [`DiagMat`].
 *
 **/



pub mod manucl;
pub mod manuconvenience;
pub mod calculator;


use std::hash::Hash;

use ocl::Buffer;

use noisy_float::types::{R32, R64};

/**
 * This trait signals that a type can be used as the maintype in the OpenCL computations
 * performed in this crate.
 * 
 * The only function [`to_cl_maintype`](ToCLMaintype::to_cl_maintype) returns a macro
 * that is used in the OpenCL code in order to conditionally compile it 
 * with maintype and mainvec set to the right type (currently float or double
 * resp. floatn or doublen)
 * 
 * The only implementors are currently `f32` and `f64`.
 */
pub trait ToCLMaintype {
    /**
     * To each `ToCLMaintype` there must belong a version that is hashable and
     * comparable and always representable as an OpenCL literal. 
     * This is necessaty in order to construct [DeltaMats](DeltaMat) from it,
     * that are not passed as arguments to kernels, but directly hardcoded
     * into a piece of OpenCL code.
     * 
     * This version of a `ToCLMaintype` must be given as `NonNaN`.
     */
    type NonNaN: Clone + PartialEq + PartialEq + Eq + Hash + CLDisplay;
    /**
     * Returns a C Macro that controls to which type (float or double
     * reps. floatn or doublen) maintype and mainvec are set in the OpenCL code.
     */
    fn to_cl_maintype() -> String;
    /**
     * Returns the name of the corresponding OpenCL type. 
     * 
     * Like `float` or `double`. 
     * */
    fn cl_type() -> String;
}

/**
 * This trait provides a way to display a number as an OpenCL literal
 * with the right type suffix (e.g. f or none).
 */
pub trait CLDisplay {
    /**
     * Converts a number to its OpenCL literal
     */
    fn cl_display(& self) -> String;
}

impl ToCLMaintype for f32 {
    type NonNaN = R32;
    fn to_cl_maintype() -> String {
        String::from("MAINTYPE_FLOAT")
    }
    fn cl_type() -> String {
        String::from("float")
    }
}

impl CLDisplay for R32 {
    fn cl_display(& self) -> String {
        format!("{:e}f", self)
    }
}

impl ToCLMaintype for f64 {
    type NonNaN = R64;
    fn to_cl_maintype() -> String {
        String::from("MAINTYPE_DOUBLE")
    }
    fn cl_type() -> String {
        String::from("double")
    }
}

impl CLDisplay for R64 {
    fn cl_display(& self) -> String {
        format!("{:e}", self)
    }
}



/**
 * Representation of a [Delta Matrix](index.html#about-delta-matrices) with
 * a variable number of dimensions, but fixed coefficients
 * that do not depend on the input.
 * 
 * Currently there are three variants for a one- two- or three-dimensional
 * delta matrix. The matrix then itself is represented as a vector
 * of "Deltas", i.e. tuples consisting of a [`Float`]-like type for the 
 * coefficient and a further tuple for the offsets in each dimension.
 * 
 * # See also
 * 
 * [`DiagMat`]
 * 
 * [`Float`]: num_traits::Float
 */
#[derive(Clone, PartialEq, Eq, Hash)]
pub enum DeltaMat<T: Clone + Eq + Hash> {
    One(Vec<(T, (i32,))>),
    Two(Vec<(T, (i32, i32))>),
    Three(Vec<(T, (i32, i32, i32))>),
}

impl<T: CLDisplay + Clone + Eq + Hash> DeltaMat<T> {
    /**
     * The number of deltas in the matrix.
     */
    pub fn len(& self) -> usize {
        match self {
            DeltaMat::One(s) => s.len(),
            DeltaMat::Two(s) => s.len(),
            DeltaMat::Three(s) => s.len()
        }
    }
    /**
     * The number of dimensions in the `DeltaMat`.
     */
    pub fn dim_count(& self) -> u32 {
        match self {
            DeltaMat::One(_) => 1,
            DeltaMat::Two(_) => 2,
            DeltaMat::Three(_) => 3
        }
    }
    /**
     * Returns the matrix in OpenCL-hardcoded form
     * 
     * A piece of OpenCL code is returned that declares and initializes
     * a variable `m` of type `delta` in the `__constant` adress space,
     * that is set to the OpenCL-version of this `DeltaMat`.
     * 
     * The output will be a `String` of the kind
     * 
     * ```OpenCL C
     * __constant delta m[len] = {deltas};
     * ```
     */
    pub fn to_cl_deltamat(& self) -> String {
        match self {
            DeltaMat::One(s) => {
                let mut res = format!("__constant delta m[{}] = {{", s.len());
                for it in s {
                    res.push_str(format!(
                        "{{{}, {{{}}}}}, ", 
                        it.0.cl_display(), 
                        (it.1).0).as_str()
                    );
                }
                res.push_str("};");
                res
            }
            DeltaMat::Two(s) => {
                let mut res = format!("__constant delta m[{}] = {{", s.len());
                for it in s {
                    res.push_str(format!(
                        "{{{}, {{{}, {}}}}}, ",  
                        it.0.cl_display(),
                        (it.1).0, (it.1).1).as_str()
                    );
                }
                res.push_str("};");
                res
            }
            DeltaMat::Three(s) => {
                let mut res = format!("__constant delta m[{}] = {{", s.len());
                for it in s {
                    res.push_str(format!(
                        "{{{}, {{{}, {}, {}}}}}, ",  
                        it.0.cl_display(),
                        (it.1).0, (it.1).1, (it.1).2).as_str()
                    );
                }
                res.push_str("};");
                res
            }
        }
    }
}

/**
 * Representation of a [Delta Matrix](index.html#about-delta-matrices) with
 * a variable number of dimensions, whose coefficients depend on
 * the index -- they are represented by an OpenCL buffer.
 * 
 * Currently there are three variants for a one- two- or three-dimensional
 * delta matrix. The matrix then itself is represented as a vector
 * of "Diags", i.e. tuples consisting of an [`ocl::Buffer`] 
 * and a further tuple for the offsets in each dimension.
 * 
 * # See also
 * 
 * [`DeltaMat`]
 * 
 * [`Float`]: num_traits::Float
 */
#[derive(Clone)]
pub enum DiagMat<T: ocl::OclPrm> {
    One(Vec<(Buffer<T>, (i32,))>),
    Two(Vec<(Buffer<T>, (i32, i32))>),
    Three(Vec<(Buffer<T>, (i32, i32, i32))>),
}

/**
 * A  without the OpenCL buffers. This is
 * kind of a "hashable" variant of [`DiagMat`].
 */
#[derive(Clone, PartialEq, Eq, Hash)]
pub enum DiagMatConfig {
    One(Vec<(i32,)>),
    Two(Vec<(i32, i32)>),
    Three(Vec<(i32, i32, i32)>),
}

impl<T: ocl::OclPrm + ToCLMaintype> DiagMat<T> {
    /**
     * The number of deltas in the matrix.
     */
    pub fn len(& self) -> usize {
        match self {
            DiagMat::One(s) => s.len(),
            DiagMat::Two(s) => s.len(),
            DiagMat::Three(s) => s.len()
        }
    }
    /**
     * The number of dimensions in the `DiagMat`.
     */
    pub fn dim_count(& self) -> u32 {
        match self {
            DiagMat::One(_) => 1,
            DiagMat::Two(_) => 2,
            DiagMat::Three(_) => 3
        }
    }

    /**
     * Returns a `vec` of the buffers of which the `DiagMat` consists. 
     * */
    pub fn buffers(&self) -> Vec<& Buffer<T>> {
        let mut res = Vec::new();
        match self {
            DiagMat::One(s) => {
                for it in s {
                    res.push(&it.0);
                }
                res
            }
            DiagMat::Two(s) => {
                for it in s {
                    res.push(&it.0);
                }
                res
            }
            DiagMat::Three(s) => {
                for it in s {
                    res.push(&it.0);
                }
                res
            }

        }
    }

    /**
     * Returns the DiagMatConfig that belongs to this DiagMat. 
     * 
     * This is the essential part of the DiagMat that is needed
     * for hashing, namely a DiagMat with the buffers left away
     * As they are passed to the vecmul_diag kernel as arguments in every
     * call, so for two DiagMats that only differ in their buffers
     * the same OpenCL-hardcoded version can be used.
     * */
    fn to_diag_mat_config(&self) -> DiagMatConfig {
        match self {
            DiagMat::One(s) => {
                let mut res = Vec::new();
                for it in s {
                    res.push(it.1);
                }
                DiagMatConfig::One(res)
            }
            DiagMat::Two(s) => {
                let mut res = Vec::new();
                for it in s {
                    res.push(it.1);
                }
                DiagMatConfig::Two(res)
            }
            DiagMat::Three(s) => {
                let mut res = Vec::new();
                for it in s {
                    res.push(it.1);
                }
                DiagMatConfig::Three(res)
            }
        }
    }
    /**
     * Returns the [`DiagMatConfig`] that belongs to this
     * `DiagMat` in OpenCL-hardcoded form.
     * 
     * A piece of OpenCL code is returned that declares and initializes
     * a variable `diagtup` of type `dimtup` in the `__constant` adress space,
     * that is set to the OpenCL-version of the `DiagMatConfig`.
     * 
     * The output will be a `String` of the kind
     * 
     * ```OpenCL C
     * __constant dimtup diagtup[len] = {dimtups};
     * ```
     */
    pub fn to_cl_dimtup(& self) -> String {
        match self {
            DiagMat::One(s) => {
                let mut res = format!("__constant dimtup diagtup[{}] = {{", s.len());
                for it in s {
                    res.push_str(format!(
                        "{{{{{}}}}}, ",  
                        (it.1).0).as_str()
                    );
                }
                res.push_str("};");
                res
            }
            DiagMat::Two(s) => {
                let mut res = format!("__constant dimtup diagtup[{}] = {{", s.len());
                for it in s {
                    res.push_str(format!(
                        "{{{{{}, {}}}}}, ",
                        (it.1).0, (it.1).1).as_str()
                    );
                }
                res.push_str("};");
                res
            }
            DiagMat::Three(s) => {
                let mut res = format!("__constant dimtup diagtup[{}] = {{", s.len());
                for it in s {
                    res.push_str(format!(
                        "{{{{{}, {}, {}}}}}, ",  
                        (it.1).0, (it.1).1, (it.1).2).as_str()
                    );
                }
                res.push_str("};");
                res
            }
        }
    }

    /**
     * Returns a comma separated list of kernel argument declarations
     * of a pointer to a maintype in the global adress space called diagarg
     * with its index as suffix. 
     * 
     * So this will look like
     * ```OpenCL C
     * __global maintype * diagarg0, __global maintype * diagarg1, …
     * ```
     */
    pub fn to_cl_kernel_args(& self) -> String {
        match self {
            DiagMat::One(s) => {
                let mut res = String::new();
                for i in 0..s.len() {
                    res.push_str(format!(
                        "__global maintype * diagarg{}, ",  
                        i).as_str()
                    );
                }
                res
            }
            DiagMat::Two(s) => {
                let mut res = String::new();
                for i in 0..s.len() {
                    res.push_str(format!(
                        "__global maintype * diagarg{}, ",   
                        i).as_str()
                    );
                }
                res
            }
            DiagMat::Three(s) => {
                let mut res = String::new();
                for i in 0..s.len() {
                    res.push_str(format!(
                        "__global maintype * diagarg{}, ",    
                        i).as_str()
                    );
                }
                res
            }
        }
    }

    /**
     * Returns OpenCL C code that declares a diagmat diagm
     * which is composed of diagtup, an array of the "diagargs" (see
     * [`to_cl_kernel_args`](DiagMat::to_cl_kernel_args)) and the
     * number of diagargs. 
     * 
     * Look into the source to see how the output looks like.
     */
    pub fn create_cl_diagmat(&self) -> String {
        let size = self.len();
        let mut res = String::from(format!("__global maintype *diagentries[{}] = {{", size));
        for i in 0..size {
            res.push_str(format!("diagarg{}, ", i).as_str());
        }
        res.push_str("};\n");
        res.push_str(format!("diagmat diagm = {{diagtup, diagentries, {} }};\n", size).as_str());
        res
    }
}

#[cfg(test)]
mod tests {
use crate::manucl::OpenCLInitialization;
pub fn default_openclini() -> Result<OpenCLInitialization, ocl::Error> {
    OpenCLInitialization::from_info(ocl::flags::DeviceType::GPU, "", "")
}
    use noisy_float::types::{R32, R64};

    use crate::DeltaMat;
    #[test]
    fn delta_mat() {
        let del = DeltaMat::Two(vec![
            (R32::new(1.5), (-2, -17)), (R32::new(-0.25), (-2, -16)),
            (R32::new(-3.0), (-1, -1)), (R32::new(2.0), (-1, 0)), (R32::new(1.5), (-1, 1)),
            (R32::new(-1.0), (0, 1)), (R32::new(1.0), (1, 1)), (R32::new(-0.125), (2, 1,)),
            (R32::new(-1.0), (4, 3)), (R32::new(1.5), (0, 0)),
        ]);
        assert_eq!(
            "__constant delta m[10] = {{1.5e0f, {-2, -17}}, {-2.5e-1f, {-2, -16}}, \
            {-3e0f, {-1, -1}}, {2e0f, {-1, 0}}, {1.5e0f, {-1, 1}}, {-1e0f, {0, 1}}, \
            {1e0f, {1, 1}}, {-1.25e-1f, {2, 1}}, {-1e0f, {4, 3}}, {1.5e0f, {0, 0}}, };", 
            del.to_cl_deltamat(),
            "Wrong representation of R32-DeltaMat"
        );

        let del = DeltaMat::Two(vec![
            (R64::new(1.5), (-2, -17)), (R64::new(-0.25), (-2, -16)),
            (R64::new(-3.0), (-1, -1)), (R64::new(2.0), (-1, 0)), (R64::new(1.5), (-1, 1)),
            (R64::new(-1.0), (0, 1)), (R64::new(1.0), (1, 1)), (R64::new(-0.125), (2, 1,)),
            (R64::new(-1.0), (4, 3)), (R64::new(1.5), (0, 0)),
        ]);
        assert_eq!(
            "__constant delta m[10] = {{1.5e0, {-2, -17}}, {-2.5e-1, {-2, -16}}, \
            {-3e0, {-1, -1}}, {2e0, {-1, 0}}, {1.5e0, {-1, 1}}, {-1e0, {0, 1}}, \
            {1e0, {1, 1}}, {-1.25e-1, {2, 1}}, {-1e0, {4, 3}}, {1.5e0, {0, 0}}, };", 
            del.to_cl_deltamat(),
            "Wrong representation of R64-DeltaMat"
        );
    }
}