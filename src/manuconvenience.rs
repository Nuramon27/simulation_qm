/* simulation_qm
 * Copyright 2018 Manuel Simon
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 * */

use std::ops::{Mul, MulAssign};


/// Returns the highest power of two that is lower or equal to x.
/// Respects the sign of x, i.e. if x is negative, the negative of the highest power
/// of 2 is returned, which is lower or equal to -x.
/// Returns 0 if x = 0.
///
/// # Examples
/// ```
/// use simulation_qm::manuconvenience::*;
/// assert_eq!(round_to_floor_p2_signed(6), 4);
/// assert_eq!(round_to_floor_p2_signed(-7), -4);
/// ```
pub fn round_to_floor_p2_signed<T: From<i8> + PartialOrd + Copy + Mul<T, Output = T> + MulAssign<T>>(x: T) -> T {
    if x > T::from(0i8) {
        let mut res: T = T::from(1i8);
        while res*T::from(2i8) <= x {
            res *= T::from(2i8);
        }
        res
    } else if x < T::from(0i8) {
        let mut res: T = T::from(-1i8);
        while res*T::from(2i8) >= x {
            res *= T::from(2i8);
        }
        res
    } else {
        T::from(0i8)
    }
}

/// Returns the highest power of two that is lower or equal to x.
/// Returns 0 if x = 0.
///
/// # Examples
/// ```
/// use simulation_qm::manuconvenience::*;
/// assert_eq!(round_to_floor_p2(257), 256);
/// assert_eq!(round_to_floor_p2(16), 16);
/// ```
pub fn round_to_floor_p2<T: From<u8> + PartialOrd + Copy + Mul<T, Output = T> + MulAssign<T>>(x: T) -> T {
    if x > T::from(0u8) {
        let mut res: T = T::from(1u8);
        while res*T::from(2u8) <= x {
            res *= T::from(2u8);
        }
        res
    } else {
        T::from(0u8)
    }
}

/// Returns the lowest power of two that is greater or equal to x.
/// Respects the sign of x, i.e. if x is negative, the negative of the lowest power
/// of 2 is returned, which is higher or equal to -x.
///
/// # Examples
/// ```
/// use simulation_qm::manuconvenience::*;
/// assert_eq!(round_to_ceil_p2_signed(6), 8);
/// assert_eq!(round_to_ceil_p2_signed(-2000), -2048);
/// assert_eq!(round_to_ceil_p2_signed(0), 1);
/// ```
pub fn round_to_ceil_p2_signed<T: From<i8> + PartialOrd + Copy + Mul<T, Output = T> + MulAssign<T>>(x: T) -> T {
    if x > T::from(0i8) {
        let mut res: T = T::from(1i8);
        while res < x {
            res *= T::from(2i8);
        }
        res
    } else if x < T::from(0i8) {
        let mut res: T = T::from(-1i8);
        while res > x {
            res *= T::from(2i8);
        }
        res
    } else {
        T::from(1i8)
    }
}

/// Returns the lowest power of two that is greater or equal to x.
///
/// # Examples
/// ```
/// use simulation_qm::manuconvenience::*;
/// assert_eq!(round_to_ceil_p2(257), 512);
/// assert_eq!(round_to_ceil_p2(0), 1);
/// assert_eq!(round_to_ceil_p2(16), 16);
/// ```
pub fn round_to_ceil_p2<T: From<u8> + PartialOrd + Copy + Mul<T, Output = T> + MulAssign<T>>(x: T) -> T {

    let mut res: T = T::from(1u8);
    while res < x {
        res *= T::from(2u8);
    }
    res
}

#[cfg(test)]
mod tests {
use super::*;
    #[test]
    fn round_to_p2() {
        assert_eq!(round_to_floor_p2(6), 4);
        assert_eq!(round_to_floor_p2(16), 16);
        assert_eq!(round_to_floor_p2(257), 256);
        assert_eq!(round_to_floor_p2(0), 0);
        assert_eq!(round_to_floor_p2(1), 1);

        assert_eq!(round_to_floor_p2_signed(6), 4);
        assert_eq!(round_to_floor_p2_signed(-7), -4);
        assert_eq!(round_to_floor_p2_signed(-16), -16);
        assert_eq!(round_to_floor_p2_signed(0), 0);
        assert_eq!(round_to_floor_p2_signed(-2000), -1024);

        assert_eq!(round_to_ceil_p2(6), 8);
        assert_eq!(round_to_ceil_p2(16), 16);
        assert_eq!(round_to_ceil_p2(257), 512);
        assert_eq!(round_to_ceil_p2(0), 1);
        assert_eq!(round_to_ceil_p2(1), 1);

        assert_eq!(round_to_ceil_p2_signed(6), 8);
        assert_eq!(round_to_ceil_p2_signed(-7), -8);
        assert_eq!(round_to_ceil_p2_signed(-16), -16);
        assert_eq!(round_to_ceil_p2_signed(0), 1);
        assert_eq!(round_to_ceil_p2_signed(-2000), -2048);
    }
}
