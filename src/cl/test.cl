/* simulation_qm
 * Copyright 2018 Manuel Simon
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 * */

#ifdef MAINTYPE_DOUBLE
    #define maintype double
#else
    /**
     * @brief The main type that is used for calculations.
     * 
     * By default a float, but if MAINTYPE_DOUBLE is defined, maintype is a double.
     **/
    #define maintype float
#endif//MAINTYPE_DOUBLE

__kernel void fill_one_d(__global maintype *dest)
{
    maintype x = (maintype)(get_global_id(0u));
    dest[get_global_id(0u)] = sin(x)*cos(x/(maintype)(2.7));
}

__kernel void fill_two_d(__global maintype *dest)
{
    maintype x = (maintype)(get_global_id(0u));
    maintype y = (maintype)(get_global_id(1u));
    *(dest + get_global_size(1u)*get_global_id(0u) + get_global_id(1u)) = sin(x)*cos(y) + 1.f;
}