/* simulation_qm
 * Copyright 2018 Manuel Simon
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 * */

/**
 * @mainpage The linear algebra kernels are basically a collection
 * for computing images of arbitrary rational functions of sparse matrices
 * that are given in a banded form.
 *
 * The basic functions are vecmul_delta() and vecmul_diag() which compute
 * a simple matrix vector product.
 *
 * Systems of linear equations given by dense matrices can be solved by linear_solve_dense().
 * */

#ifndef dimension
    /**
     * @brief The vector dimension for which vecmul_delta() and vecmul_diag()
     * are compiled.
     *
     * May be 1, 2 or 3.
     * */
    #define dimension 2u
#endif//dimension
#ifndef VECSIZE
    /**
     * @brief The main vector size that shall be used in calculations.
     *
     * Determines the size of #mainvec. May be 1, 2, 4, 8, 16. A good recommendation
     * is the preferred vector width of the device.
     * */
    #define VECSIZE 8u
#endif//VECSIZE
#ifndef LOC
    #define LOC 0
#endif//LOC
#ifdef MAINTYPE_DOUBLE
    #define maintype double
#else
    /**
     * @brief The main type that is used for calculations.
     * 
     * By default a float, but if MAINTYPE_DOUBLE is defined, maintype is a double.
     **/
    #define maintype float
#endif//MAINTYPE_DOUBLE

#if (VECSIZE == 0x1u)
    #define VLOAD_FUNC(offset, p) p[offset]
    #define VSTORE_FUNC(data, offset, p) p[offset] = data;
    #ifdef MAINTYPE_DOUBLE
        #define mainvec double
    #else
        #define mainvec float
    #endif//MAINTYPE_DOUBLE

    __constant ushort mainvecsize = 0x1u;

#elif (VECSIZE == 0x2u)
    #define VLOAD_FUNC(offset, p)  vload2(offset, p)
    #define VSTORE_FUNC(data, offset, p) vstore2(data, offset, p)
    #ifdef MAINTYPE_DOUBLE
        #define mainvec double2
    #else
        #define mainvec float2
    #endif//MAINTYPE_DOUBLE

    __constant ushort mainvecsize = 0x2u;
    __constant uint4 shufflemask = (uint4)(0x0u, 0x1u, 0x2u, 0x3u);


#elif (VECSIZE == 0x4u)
    #define VLOAD_FUNC(offset, p)  vload4(offset, p)
    #define VSTORE_FUNC(data, offset, p) vstore4(data, offset, p)
    #ifdef MAINTYPE_DOUBLE
        #define mainvec double4
    #else
        #define mainvec float4
    #endif//MAINTYPE_DOUBLE

    __constant ushort mainvecsize = 0x4u;
    __constant uint8 shufflemask = (uint8)(0x0u, 0x1u, 0x2u, 0x3u, 0x4u, 0x5u, 0x6u, 0x7u);


#elif (VECSIZE == 0x8u)
    /**
     * @brief A generic vload-function, that loads a vector of size #mainvecsize
     * and can also load a single #maintype.
     **/
    #define VLOAD_FUNC(offset, p)  vload8(offset, p)
    /**
     * @brief A generic vstore-function, that stores a vector of size #mainvecsize
     * and can also store a single #maintype.
     * */
    #define VSTORE_FUNC(data, offset, p) vstore8(data, offset, p)
    #ifdef MAINTYPE_DOUBLE
        #define mainvec double8
    #else
    /**
     * @brief The main vector type that is used for calculations.
     * It is made out of #mainvecsize elements of #maintype.
     * */
        #define mainvec float8
    #endif//MAINTYPE_DOUBLE

    /**
     * @brief The size of #mainvec. Identical to VECSIZE.
     * */
    __constant ushort mainvecsize = 0x8u;
    /**
     * @brief The shufflemask are 2*#mainvecsize consecutive integers beginning with zero,
     * that are used in load_misal_vec() in order to make one #mainvec out of the right components
     * of two mainvecs, using shuffle functions.
     * */
    __constant uint16 shufflemask = (uint16)(0x0u, 0x1u, 0x2u, 0x3u, 0x4u, 0x5u, 0x6u, 0x7u,
                                               0x8u, 0x9u, 0xAu, 0xBu, 0xCu, 0xDu, 0xEu, 0xFu);


#elif (VECSIZE == 16u)
    #define VLOAD_FUNC(offset, p)  vload16(offset, p)
    #define VSTORE_FUNC(data, offset, p) vstore16(data, offset, p)
    #ifdef MAINTYPE_DOUBLE
        #define mainvec double16
    #else
        #define mainvec float16
    #endif//MAINTYPE_DOUBLE

    typedef uint16 mainint;
    __constant ushort mainvecsize = 0x10u;
    /**
     * @brief The shufflemask are #mainvecsize consecutive integers beginning with zero,
     * (similar to #shufflemask)
     * that are used in load_misal_vec() in order to make one #mainvec out of the right components
     * of two mainvecs, using shuffle functions, for the special case
     * #mainvecsize == 16, where there is no vector of size 2*#mainvecsize,
     * so a different mechanism must be used.
     * */
    __constant uint16 shufflemask_half = (uint16)(0x0u, 0x1u, 0x2u, 0x3u, 0x4u, 0x5u, 0x6u, 0x7u,
                                                  0x8u, 0x9u, 0xAu, 0xBu, 0xCu, 0xDu, 0xEu, 0xFu);
#else
    #error VECSIZE must be 1, 2, 4, 8 or 16!
#endif//VECSIZE



/**
 * @section About delta matrices.
 * The functions in this file work with a very simple form of sparse matrices,
 * the so-called delta matrices. These are matrices whose components can be written
 * as a linear combination of Kronecker Deltas, possibly multiplied by a further fucntion of the
 * index. The indices on the Kronecker Delta is considered to have the form
 * d_{i+del, j} (so there may not be a factor before the i or the j).
 * E.g. the discrete derivative is a delta matrix as it can be written as
 * d_{i+1, j} - d_{i-1, j}. If you write the components of a vector A_i at the
 * diagonal entries of a matrix, you get a delta matrix, too: A_i d_{i, j},
 * where the Kronecker delta is multiplied with a function of the index.
 *
 * The specialty of the functions in this file is that they can not only
 * handle matrices that describe linear maps of one-dimensional vectors,
 * but also of tensor of second or third (...) stage, which is necessary
 * to solve PDEs in more than one dimension by discretization. Those higher
 * dimensional matrices have then, if dim is the dimension, 2*dim indices
 * and for example the product of a "three-dimensional" matrix M_{ijklmn} with a three
 * dimensional vector v_{jln} is then M_{ijklmn}*v_{jln} so you sum up over
 * every second index of the matrix. The higher dimensional delta matrixes
 * are then a linear combination of products of dim Kronecker-deltas.
 * E.g the Laplacian in two dimensions is then (in its simplest form)
 * L_{ijkl} = d_{i+1,j}d_{k,l} + d_{i-1,j}d_{k,l}
 * - 4d_{i,j}d_{k,l} + d{i,j}d_{k+1,l} + d{i,j}d_{k-1,1}
 *
 **/

typedef struct _dimtup
{
    int v[dimension];
} dimtup;
/**
 * A delta with a fixed coefficient that does not depend on the index
 */
typedef struct _delta
{
    maintype val;
    int del[dimension];
} delta;

/**
 * A delta with a coefficient that does depend on the index
 */
typedef struct _diag
{
    __global maintype *val;
    int del[dimension];
} diag;

typedef struct _deltamat
{
    __constant delta *entry;
    unsigned int size;
} deltamat;

typedef struct _diagmat
{
    __constant dimtup* del;
    maintype __global * __private *val;
    unsigned int size;
} diagmat;


typedef struct _divrem
{
    size_t div;
    size_t rem;
} divrem;

/**
 * @brief Calculates the linear global index, according to #dimension and the global work size.
 *
 * In an array which has the global work sizes as dimensions, this is the linear
 * index on which to find the item (get_global_id(0), get_global_id(1), ...) in memory.
 **/
size_t calc_index()
{
    size_t res = 0u;
    size_t factor = 1u;
    for (char dimindx = dimension-1; dimindx >= 0; --dimindx)
    {
        res += factor * get_global_id(dimindx);
        factor *= get_global_size(dimindx);
    }

    return res;
}

/**
 * @brief Calculates the linear local index, according to #dimension and the local work size.
 *
 * In an array which has the local work sizes as dimensions, this is the linear
 * index on which to find the item (get_local_id(0), get_local_id(1), ...) in memory.
 **/
size_t calc_index_loc()
{
    size_t res = 0u;
    size_t factor = 1u;
    for (char dimindx = dimension-1; dimindx >= 0; --dimindx)
    {
        res += factor * get_local_id(dimindx);
        factor *= get_local_size(dimindx);
    }

    return res;
}

/**
 * @brief Calculates the linear global index of the array indices, according to #dimension and
 * the global work size.
 * @param indices Contains the indices for each axis. Must have (at least) size #dimension.
 *
 * This function can be used for calculating differences in global indices.
 * For expalanation of the linear global index, see calc_index().
 **/
int calc_index_signed_single(__constant int *indices)
{
    int res = 0;
    long factor = 1L;
    for (char dimindx = dimension-1; dimindx >= 0; --dimindx)
    {
        res += factor * indices[dimindx];
        factor *= get_global_size(dimindx);
    }

    return res;
}

/**
 * @brief Calculates the linear local index of the array indices, according to #dimension and
 * the local work size.
 * @param indices Contains the indices for each axis. Must have (at least) size #dimension.
 *
 * This function can be used for calculating differences in local indices.
 * For expalanation of the linear local index, see calc_index().
 **/
int calc_index_signed_single_loc(__constant int *indices)
{
    int res = 0;
    long factor = 1L;
    for (char dimindx = dimension-1; dimindx >= 0; --dimindx)
    {
        res += factor * indices[dimindx];
        factor *= get_local_size(dimindx);
    }

    return res;
}

/**
 * @brief Calculates the linear index represented by the array indices
 * after dividing the last index by mainvecsize
 * (and rounded to the next lower integer, i.e. -3/8 -> -1),
 * according to #dimension and the global work size.
 * @param indices Contains the indices for each axis. Must have (at least) size #dimension,
 * but the size must also be at least one.
 *
 * When you have an array of type mainvec (i.e. vectors of type maintype and size mainvecsize)
 * and want to load a vector from a position relative to the global index,
 * this function is the right one. Global index gives in this case
 * a position in the field of vectors, while indices gives a relative position
 * measured in single elements of type maintype (i.e. if the last index of
 * indices is 2 and mainvecsize is 8, the same vector should be loaded
 * as if maintype was 0).
 *
 * This means that the last element of indices should be divided by
 * mainvecsize (rounded to the next lower integer), before
 * calculating i relative linear index with it.
 **/
int calc_index_from_signed_vec(__constant int * indices)
{
    int res = indices[dimension-1u];
    /* The integer division of the last index by mainvecsize (rounded to the
     * next lower integer) is calculated by first subtracting
     * its euclidean modulus and then dividing by mainvecsize
     * (after subtraction of the modulus, it is dividable by mainvecsize).
     * The modulus is calculated in a sign indebendend way by
     * &-ing with mainvecsize-1u, this works in binary system
     * becaus mainvecsize is a power of two
     * (so res & (mainvecsize-1u) are just the last (log (mainvecsize))
     * places of res).
     **/
    res = (res - (int)(res & (mainvecsize - 1u)))/(int)(mainvecsize);
    long factor = get_global_size(dimension-1u);
    /* Only the last dimension has to be handled in a special way
     * because of the vectorial nature of the indices,
     * all other dimensions stay the same.
     **/
    for (char dimindx = dimension-2; dimindx >= 0; --dimindx)
    {
        res += factor * indices[dimindx];
        factor *= get_global_size(dimindx);
    }

    return res;
}

/*
 * @brief Calculates the linear index represented by the array indices
 * after dividing the last index by mainvecsize
 * (and rounded to the next lower integer, i.e. -3/8 -> -1),
 * according to #dimension and the global work size.
 * @param indices Contains the indices for each axis. Must have (at least) size #dimension,
 * but the size must also be at least one.
 *
 * The same as calc_index_from_signed_vec() but locally.
 **/
int calc_index_from_signed_vec_loc(__constant int * indices)
{
    // For commentary see calc_index_from_signed_vec
    int res = indices[dimension-1u];
    res = (res - (int)(res & (mainvecsize - 1u)))/(int)(mainvecsize);
    long factor = get_local_size(dimension-1u);
    for (char dimindx = dimension-2; dimindx >= 0; --dimindx)
    {
        res += factor * indices[dimindx];
        factor *= get_local_size(dimindx);
    }

    return res;
}

/**
 * @brief Performs a scalar product of two mainvecs.
 * @param left The left side of the scalar product
 * @param right The right side of the scalar product
 *
 * If VECSIZE is lower than 8, it simply calls dot(). If VECSIZE is 8 or 16,
 * it calls dot() on quarters or halfs of the vecs and sums up the results.
 **/
maintype mdot(mainvec left, mainvec right)
{
#if (VECSIZE == 0x1u || VECSIZE == 0x2u || VECSIZE == 0x4u)
    return dot(left, right);
#elif (VECSIZE == 0x8u)
    return dot(left.lo, right.lo) + dot(left.hi, right.hi);
#elif (VECSIZE == 0x10u)
    return dot(left.lo.lo, right.lo.lo) + dot(left.lo.hi, right.lo.hi)
           + dot(left.hi.lo, right.hi.lo) + dot(left.hi.hi, right.hi.hi);
#endif//VECSIZE
}

#if (VECSIZE > 0x1u)
/**
 * @brief Loads a #mainvec from a position that is not aligned to #mainvecsize, where
 * only those components are loaded, that lie inside a boundary
 * @param delta The offset of the vector to load, counted in maintypes
 * @param right The buffer from which a mainvec should be loaded
 *
 * The position, from which the vector is loaded, is given by the global ids
 * with an offset of delta in the last dimension (counted in maintypes).
 *
 * The function loads two mainvecs from aligned positions, such that both
 * together cover the final #mainvec to load, and then uses shuffle functions
 * in order to extract the right components from them.
 **/
mainvec load_misal_vec_global(__constant int *delta, __global maintype *right)
{
    /* As the delta
     * in the last dimension will not
     * always be a multiple of mainvecsize, the elements that must be loaded
     * from the source will not lie in one mainvec, but in between of two mainvecs.
     * For example if #mainvecsize = 4 and del=1, we may get the following situation
     * -*** *---
     * where * are the elements that must be loaded. As we can only
     * load mainvecs from an aligned address, we must load two mainvecs
     * and then collect the correct elements of this mainvec.
     *
     * Be aware that this is only important for the last dimension (as long as
     * the last global size is a multiple of mainvecsize)
     **/
    size_t index = calc_index();
    /* As all dimensions but the last can be treated normally,
     * for these we can check if the source elements corresponding
     * to the delta and the destination of this work-item
     * lie inside the vector in normal way,
     * by checking whether the corresponging index lies inside
     * the global range.
     **/
    bool in_bounds = true;
    for (uchar j = 0u; j < dimension-1u; ++j)
       in_bounds = in_bounds && ((long) get_global_id(j) + delta[j] >= 0L)
                             && ((long) get_global_id(j) + delta[j] < get_global_size(j));
    if (in_bounds)
    {
        int del = delta[dimension-1u];
        /* If the del in the last dimension is a multiple of the mainvecsize
         * we can load one single vector and treat it normally.
         **/
        if ((del & (mainvecsize - 1u)) == 0)
        {
            /* But we still have to check whether the index in the last dimension
             * lies in the global range.
             **/
            if (((long) (get_global_id(dimension-1u)*mainvecsize) + del
                          >= 0L) &&
                ((long) (get_global_id(dimension-1u)*mainvecsize) + del
                          < get_global_size(dimension-1u)*mainvecsize))
               return VLOAD_FUNC((long)(index) + calc_index_from_signed_vec(delta), right);
           else
            return (mainvec) (0.0f);
        }
        else
        {
            /* Otherwise two consecutive vecs must first be loaded from
             * the source which will be stored in rightvec1 and rightvec2.
             * We call the vec that will be created from these two vecs
             * (and which holds the source elements acutally needed) the final vec.
             * The index of the first vec (in the last dimension) relative to
             * this item's index is the del divided by mainvecsize but rounded
             * to the next lower integer, because of this del is -1,
             * we must go "one step back" in order to obtain the first element
             * that we need. The function calc_index_from_signed_vec does
             * this for us.
             *
             * But for both vecs we need to check, whether they lie
             * in the global range. (E.g. for the last work-item computing a
             * del of +1 it must only the rightvec1, and the last element of
             * the final vec shall be substituted by a zero, so
             * case we can create the final vec in the same way from the rightvecs
             * when setting rightvec2 to zero). Here we could also divide del
             * by mainvecsize and round to the next lower integer,
             * then add it to the global id in the last dimension
             * and compare it with the global range, but it is easier to leave
             * the del as it is and instead multiply the global_ids
             * and global sizes by mainvecsize.
             *
             * rightvec2 lies one complete mainvec behind rightvec1, so here one is
             * added to the global id befor multiplying it with mainvecsize.
             *
             * For each rightvec, if its index lies outside the global range
             * it is substituted by a zero vec.
             **/
            mainvec rightvec1 =
                    (((long) (get_global_id(dimension-1u)*mainvecsize) + del
                              >= 0L) &&
                    ((long) (get_global_id(dimension-1u)*mainvecsize) + del
                            < get_global_size(dimension-1u)*mainvecsize))?
                    VLOAD_FUNC(
                        (long)(index) + calc_index_from_signed_vec(delta), right
                    ) : (0.f);
            mainvec rightvec2 =
                    (((long) ((get_global_id(dimension-1u) + 1u)*mainvecsize) + del
                               >= 0L) &&
                     ((long) ((get_global_id(dimension-1u) + 1u)*mainvecsize) + del
                               < get_global_size(dimension-1u)*mainvecsize))?
                    VLOAD_FUNC(
                        (long)(index) + calc_index_from_signed_vec(delta) + 1L, right
                    ) : (0.f);

#if (VECSIZE == 16u)
            mainint mask2;
            mask2.even = (shufflemask_half.lo
                          + (uint) ((mainvecsize >> 0x1u) * ((uint)(del) & 0x1u)));
            mask2.odd  = (shufflemask_half.hi
                          + (uint) ((mainvecsize >> 0x1u) * ((uint)(del) & 0x1u)));
            return shuffle2(
                shuffle2(rightvec1->even, rightvec2.even,
                         shufflemask_half
                         + (uint) ((((uint)(del) & (mainvecsize - 1u))+1u) >> 0x1u)).lo,
                shuffle2(rightvec1->odd , rightvec2.odd,
                         shufflemask_half
                         +  (uint) (((uint)(del) & (mainvecsize - 1u)    ) >> 0x1u)).lo,
                mask2);
#else
            /* Finally the final vec is computed. When mainvecsize is not 16,
             * we can do this by first concatenating rightvec1 and rightvec2 to
             * one vec of twice the size and then choosing the right elements.
             * We do this with the shuffle2 function: We concatenate the
             * two rightvecs and the shift the elements needed for the final vec
             * into the lower half -- and then only take the lower half of this vector.
             *
             * This shifting is done by adding the del
             * to a vec containing the elments (0, 1, 2, ..) (the shufflemask)
             * and giving this "shifted" shufflemask as the last argument
             * to shuffle2.
             *
             * When mainvesize is 16, we cannot concatenate the two rightvecs
             * because there are no vecs of size 32. Instead we invented
             * a genious way to achieve the same by handling even and odd
             * elements of the vectors separately. It will be described above.
             **/
            return shuffle2(rightvec1, rightvec2,
                              shufflemask + (uint) (del & (mainvecsize - 1u))).lo;
#endif //VECSIZE == 16u
        }
    }
    else
        return (mainvec) (0.0f);
}
#endif//VECSIZE

/**
 * @brief Adds left to right.
 * @param left A readable vector of the same shape as right
 * @param right A read- and writeable multidimensional vector.
 *
 * Itemwise, this performs the calculation right[index] += left[index].
 */
__kernel void add_to(__global maintype *left, __global maintype *right)
{
    size_t index = calc_index();
    mainvec rightvec = VLOAD_FUNC(index, right);
    VSTORE_FUNC(VLOAD_FUNC(index, left) + rightvec, index, right);
}

/**
 * @brief Performs a scalar multiplication of the vector right with factor,
 * and stores the result in right.
 * @param factor The factor with which to multiply
 * @param right A read- and writeable multidimensional vector.
 *
 * Itemwise, this performs the calculation right[index] *= factor.
 */
__kernel void scalarmul_to(maintype factor, __global maintype *right)
{
    size_t index = calc_index();
    mainvec rightvec = VLOAD_FUNC(index, right);
    VSTORE_FUNC(factor * rightvec, index, right);
}

#if (VECSIZE == 0x1u)
void vecmul_delta(
    __constant delta *left, size_t leftsize,
    __global maintype *right,
    __global maintype *dest)
{
    size_t index = calc_index();
    maintype res = 0.f;
    for (size_t i = 0u; i < leftsize; ++i)
    {
        /* Here whe check whether the element of right that is pointed to
         * by the delta for this row of the matrix, lies inside the vector,
         * i.e. if every index of this element is greater zero
         * and lower than the global work size.
         **/
        bool in_bounds = true;
        for (uchar j = 0u; j < dimension; ++j)
           in_bounds = in_bounds
                       && ((long) get_global_id(j) + left[i].del[j] >= 0L)
                       && ((long) get_global_id(j) + left[i].del[j] < get_global_size(j));

        if (in_bounds)
            res += left[i].val * right[index + calc_index_signed_single(left[i].del)];
    }
    dest[index] = res;
}
#else
/**
 * @brief Multiplies a multidimensional delta-matrix
 * with a vector in a possibly vectorized way.
 * @param left The delta-matrix with which to multiply
 * @param leftsize The number of deltas contained in left
 * @param right The source vector with which to multiply
 * @param dest The destination vector in which the result shall be written.
 * Must not be identical to right!

 * Every work-item calculates one #mainvec of elements in the destination vector,
 * so there must be (size of dest)/#mainvecsize work-items (per #dimension).
 * If #VECSIZE == 1, a simpler function without these complicated
 * vector loading function is used.
 *
 * @attention The global_work_size in the last dimension must be a multiple of mainvecsize!
 * @sa vecmul_diag(), load_misal_vec()
 **/
void vecmul_delta(
    __constant delta *left, size_t leftsize,
    __global maintype *right,
    __global maintype *dest)
{
    /* Each work-item shall now compute mainvecsize elements in the destination vector,
     * so it must also load mainvecsize elements of the source vector. As the del
     * in the last dimension will not
     * always be a multiple of mainvecsize, the elements that must be loaded
     * from the source will not lie in one mainvec, but in between of two mainvecs.
     * The loading of a vec from such a misalagned position is handled by the function
     * load_misal_vec().
     **/
    size_t index = calc_index();
    mainvec res = (mainvec)(0.f);
    for (size_t i = 0u; i < leftsize; ++i)
        res += left[i].val * load_misal_vec_global(left[i].del, right);

    VSTORE_FUNC(res, index, dest);
}
#endif//VECSIZE

#if (VECSIZE == 0x1u)
void vecmul_diag(
    diagmat left,
    __constant delta *leftdelta, unsigned int lefdeltatsize,
    __global maintype *right,
    __global maintype *dest)
{
    /* For commentary see vecmul_delta()
     **/
    size_t index = calc_index();
    maintype res = 0.f;

    // At first the delta part is computed.
    for (unsigned int i = 0u; i < leftdeltasize; ++i)
    {
        /* For commentary see vecmul_delta
         **/
        bool in_bounds = true;
        for (uchar j = 0u; j < dimension; ++j)
           in_bounds = in_bounds
                       && ((long) get_global_id(j) + leftdelta[i].del[j] >= 0L)
                       && ((long) get_global_id(j) + leftdelta[i].del[j] < get_global_size(j));

        if (in_bounds)
            res += leftdelta[i].val * right[index + calc_index_signed_single(left[i].del)];
    }
    // And then the diag part.
    for (unsigned int i = 0u; i < left.size; ++i)
    {
        bool in_bounds = true;
        for (uchar j = 0u; j < dimension; ++j)
           in_bounds = in_bounds
                       && ((long) get_global_id(j) + left.del[i].v[j] >= 0L)
                       && ((long) get_global_id(j) + left.del[i].v[j] < get_global_size(j));

        if (in_bounds)
            res += left.val[i][index] * right[index + calc_index_signed_single(left.del[i].v)];
    }
    dest[index] = res;
}
#else

/**
 * @brief Multiplies a multidimensional diag-matrix
 * with a vector in a possibly vectorized way.
 * @param left The diag-matrix with which to multiply
 * @param leftdelta An additional delta-matrix that shall alos be multiplied
 * with right, where the result is added to the result of the multiplication
 * with left.
 * @param leftsize The number of deltas contained in left
 * @param right The source vector with which to multiply
 * @param dest The destination vector in which the result shall be written.
 * Must not be identical to right!

 * Every work-item calculates one #mainvec of elements in the destination vector,
 * so there must be (size ot dest)/mainvecsize work-items (per dimension).
 * one element of the destination vector.
 * If #VECSIZE == 1, a simpler function without these complicated
 * vector loading function is used.
 *
 * Often, a diag matrix has only few diagonals in which the entry really
 * depends on the index, while the others have constant entries which can 
 * as well be represented by a delta-matrix. Due to this reason, to this
 * function an additional delta-matrix leftdelta is given and calculated is the product
 * of the sum of left and leftdelta and right.
 *
 * @attention The global_work_size in the last dimension must be a multiple of mainvecsize!
 **/
void vecmul_diag(
    diagmat left,
    __constant delta *leftdelta, unsigned int lefdeltatsize,
    __global maintype *right,
    __global maintype *dest)
{
    /* For commentary see vecmul_delta()
     **/
    size_t index = calc_index();
    mainvec res = (mainvec)(0.f);

    // At first the delta part is computed.
    for (unsigned int i = 0u; i < lefdeltatsize; ++i)
        res += leftdelta[i].val * load_misal_vec_global(leftdelta[i].del, right);
    // And then the diag part.
    for (unsigned int i = 0u; i < left.size; ++i)
            res += VLOAD_FUNC(index, left.val[i])
                   * load_misal_vec_global(left.del[i].v, right);
                   
    VSTORE_FUNC(res, index, dest);
}
#endif//VECSIZE

/**
 * @brief Does a local additive reduction of get_local_size(0u)*loc_stepsize*VECSIZE
 * mainvecs to a single one.
 * @param loc_stepsize The number of elements that shall be added in each step
 * @param ires The elements to add up.
 * Size must be loc_stepsize*get_local_size(0u) (in mainvecs)
 * @param res The buffer for the final result. Its size must be the quotient
 * of global size through local size.
 *
 * In this function every work group consecutively adds loc_stepsize mainvecs of in until
 * they are finally reduced to a single mainvec as result. The components
 * of this vector are then summed up together to a single result per work group.
 **/
void sumup_loc(ushort loc_stepsize, __local mainvec *ires, __global maintype *res)
{
    for (ushort factor = 1u; factor < get_local_size(0u)*loc_stepsize; factor *= loc_stepsize)
    {
        for (
            size_t i = factor;
            i < loc_stepsize*factor &&
            i + factor*loc_stepsize*get_local_id(0u) < get_local_size(0u)*loc_stepsize;
            i += factor
        )
        {
            ires[factor*loc_stepsize*get_local_id(0u)] +=
                ires[factor*loc_stepsize*get_local_id(0u) + i];
        }
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    /* When this function is finished, there shall be only one summand per work group,
     * so the the result is written to res + get_global_id(0)/get_local_size(0u)
     */
    if (get_local_id(0u) == 0u)
        *(res + get_global_id(0u)/get_local_size(0u)) = mdot(ires[0], (mainvec) (1.0f));
}

/**
 * @brief Performs the first, global part of a scalar product.
 * @param n The size of the vectors to multiply
 * @param loc_stepsize The number of additions that shall be performed in every step
 * when reducing the summands to one per work group.
 * @param left The left side of the product
 * @param right The right side of the product
 * @param res The buffer in which the reduced sum will be stored.
 * It must have size get_global_size(0u)/get_local_size(0u)!
 * @param ires A buffer of local memory for storing interim results.
 * Its size must be get_local_size(0u)*loc_stepsize*VECSIZE
 *
 * This function performs the first part of calculating a scalar product and is intended to
 * be used in conjunction with sumup_loc_kern(): scalar_prod_glob at first
 * reduces the sum of the componentwise products to get_global_id(0u)/get_local_id(0u),
 * using many work groups
 * and thus saturating the device. Then the rest of the
 * sum can be performed by sumup using a single work group.
 *
 * When the second part is going to be performed with loc_stepsize2 and VECSIZE2
 * at a local size of loc_size2 by sumup_loc_kern(), then
 * the number of work groups must be the product of the three:
 * loc_size2 * loc_stepsize2 * VECSIZE2
 *
 * It is sensible, that n is a multiple of the number of work groups, as well as of the local
 * size and VECSIZE
 */
__kernel void scalar_prod_glob(
    unsigned long n, ushort loc_stepsize, __global maintype *left, __global maintype *right,
    __global maintype *res, __local maintype *ires)
{
    /* In the following, targetsize = get_global_size(0u)/get_local_size(0u). This is the size
     * In a first step, each work item takes glob_stepsize vectors of VECSIZE from left and right,
     * multipies them and sums up the products, using mdot(). Here glob_stepsize
     * is ceil(n/(get_global_size(0u)*VECSIZE*VECSIZE*loc_stepsize)).
     *
     * This guarantees that after this step there are
     * get_global_size(0u)*loc_stepsize*VECSIZE elements left to sum up.
     *
     * In a second step, sumup_loc() is called, so every work group
     * reduces its get_local_size(0u)*loc_stebsize*VECSIZE elements to a single one,
     * leaving targetsize elements for sumup_loc_kern.
     **/

    size_t glob_stepsize = (ushort)(
        ceil((maintype)(n)/(get_global_size(0u)*VECSIZE*VECSIZE*loc_stepsize))
    );
    for (ushort j = 0u; j < loc_stepsize; ++j)
        VSTORE_FUNC((mainvec)(0.0f), loc_stepsize*get_local_id(0u) + j, ires);
    for (ushort j = 0u; j < loc_stepsize*VECSIZE; ++j)
    {
        for (size_t i = 0u;
        i < glob_stepsize &&
        (glob_stepsize*(VECSIZE*loc_stepsize*get_global_id(0u) + j) + i)*VECSIZE < n;
        ++i)
        {
            ires[VECSIZE*loc_stepsize*get_local_id(0u) + j] +=
                    mdot(VLOAD_FUNC(
                        glob_stepsize*(VECSIZE*loc_stepsize*get_global_id(0u) + j) + i, left
                    ),VLOAD_FUNC(
                        glob_stepsize*(VECSIZE*loc_stepsize*get_global_id(0u) + j) + i, right
                    ));
        }
    }
    sumup_loc(loc_stepsize, (__local mainvec*) (ires), res);
}

/**
 * @brief Performs the second, local part of a scalar product.
 * @param loc_stepsize The number of elements that shall be added in each step
 * @param in The elements to add up.
 * Size must be loc_stepsize*get_local_size(0u)*VECSIZE
 * @param res The buffer for the final result. Has size 1.
 * @param ires A local buffer for storing intermediate results.
 * Has a size of loc_stepsize*get_local_size(0u) mainvecs, or
 * the same size as in if counted in maintypes.
 *
 * This function basically loads one mainvec per work item
 * from in into ires and then calls sumup_loc on ires.
 **/
__kernel void sumup_loc_kern(ushort loc_stepsize, __global maintype *in,
__global maintype *res, __local mainvec *ires)
{
    for (ushort j = 0u; j < loc_stepsize; ++j)
        ires[loc_stepsize*get_local_id(0u) + j] = VLOAD_FUNC(
            loc_stepsize*get_global_id(0u) + j, in
        );

    sumup_loc(loc_stepsize, ires, res);
}

/**
 * @brief Solves the system of linear equations Ax = b.
 * @param n The size of the matrix A and the vector b.
 * @param A The matrix. Must have size n*n.
 * @param b The vector. Must have size n. 
 * @param pivot A local storage for pivots. Must have size 1.
 *
 * Uses Gaussion elimination for solving a dense SLE, but as much synchronization
 * is needed for this algorithm, it is only computed by one work group and
 * only local ids are used for adress calculation. But still it is possible to
 * give a different matrix A and/or vector b to each work group, so
 * each work group calculates a different SLE and they work completely in parallel.
 *
 * The result is stored in the buffer b.

 * It only works on invertible matrices, calling this function on a non-invertible matrix
 * will cause an error and produce no valid result.
 *
 * @attention Has side effects on A and b!
 * */
void linear_solve_dense_loc(
    ushort n, __local maintype *A, __local maintype *b, __local maintype *pivot
)
{
    /* We do Gaussian elimination, so in the firs step
     * we go through the columns (and as well through
     * the rows) and null the entries below the diagonal of each column,
     * column for column. Nulling the lower part of on column is called one round.
     * The index of this column is the current index, the column itself the current column;
     * not that this is also
     * the index of the row in which the pivot is, that will be used
     * for nulling -- the current row.
     * 
     * In the second step, we go backwards and null the upper diagonal entries of 
     * the matrix.
     *
     * The diagonal entry of the current row is called pivot.
     *
     * Calculations with obvious results (i.e. 0) or results that are never used again
     * are left out. This holds for every operation on the Matrix after
     * it is brought to row echelon form!
     *
     * The function is parallelized in the following way: Every work item
     * processes n/get_global_size(0u) rows and n/get_global_size(1u) columns.
     * These have a distance of get_global_size(0u) from each other, i.e. the elements
     * that are processed by work item (1, 2) with n = 8 and local sizes (2, 4)
     * are the following:
     *
     * - - - - - - - -
     * - + - - - + - -
     * - - - - - - - -
     * - + - - - + - -
     * - - - - - - - -
     * - + - - - + - -
     * - - - - - - - -
     * - + - - - + - -
     * 
     * These entries are stable, so in every round each work item will calculate the
     * same entries if they must be calculated in this round,
     * and also in every round there are other work items (partially) idle.
     *
     * The entries in the current row or above are not calculated in this row
     * (only those below), so the corresponding
     * work items may do other things. This is performing the row operations
     * on the vector b. The distribution of this work can be imagined as transposing
     * b and fitting it into the row zero, so the work item with get_local_id(1u) == k
     * calculates the entries k + j*get_local_size(1u) of b.
     *
     * When the matrix is in row echelon form, the remaining operations
     * must only be performed on b, because we only null entries in the matrix.
     * The work is then distributed in a one-dimensional way onto b,
     * i.e. every work item processes n/(get_local_size(0u)*get_local_size(1u)
     * entries.
     */


    /* sourceline will run from 0 to n and represent the column we are currently
     * nulling. This is also the row and column of the pivot that is currently used.
     */
    ushort sourceline;
    /* When the last column is current, the matrix is already in row echelon form
     * so we can immediately go to the second step.
     */
    for (sourceline = 0u; sourceline < n - 1u; ++sourceline)
    {
        /* At first, the work item that calculated the entry that is now the pivot
         * fetches this pivot into a local variable.
         */
        if (get_local_id(0u) == sourceline % get_local_size(0u)
            && get_local_id(1u) == sourceline % get_local_size(1u))
            *pivot = *(A + (n+1u)*sourceline);
        /* We would have to wait for the other work groups either way,
         * so storing the pivot in a local variable has no additional cost.
         */
        barrier(CLK_LOCAL_MEM_FENCE);

        if (!isnormal(*pivot) && get_local_id(0u) == sourceline % get_local_size(0u))
        {
            for (ushort k = sourceline + 1u;; ++k)
            {
                if (k == n)
                    return;

                if (isnormal(*(A + n*k)))
                {
                    for (ushort j = sourceline - (sourceline % get_local_size(1u));
                         j < n; j += get_local_size(1u))
                    {
                        if (j == sourceline - (sourceline % get_local_size(1u))
                            && get_local_id(1u) == 0u)
                        {
                            maintype source = *(b + k);
                            *(b + sourceline) -= source;
                        }
                        if (j + get_local_id(1u) >= sourceline)
                        {
                            maintype source = *(A + n*k + j + get_local_id(1u));
                            *(A + n*sourceline + j + get_local_id(1u)) -= source;
                        }
                    }
                    break;
                }
            }
        }
        if (get_local_id(0u) == sourceline % get_local_size(0u)
            && get_local_id(1u) == sourceline % get_local_size(1u))
            *pivot = *(A + (n+1u)*sourceline);
        barrier(CLK_LOCAL_MEM_FENCE);

        /* Each work item processes n/get_local_size(0u) rows
         * so we need an offset additional to the local id. This offset is i.
         * As only rows greater than sourceline must be processed,
         * we want to begin at sourcelin, but in order not to get odd offsets,
         * we round down to the next multiple of the local size.
         */
        for (ushort i = sourceline - (sourceline % get_local_size(0u));
             i < n; i += get_local_size(0u))
        {
            /* The work items with local_size(0u) == 0u calculate b
             * but they should only do this once in this loop.
             *
             * This calculation is not put before the loops,
             * because in the other runs of this loop, the corresponding
             * work item may have to do calculations on A,
             * so this would hurt parallelization.
             */
            if (i == sourceline - (sourceline % get_local_size(0u))
                && get_local_id(0u) == 0u)
            {
                /* source is always the element of the current row
                 * with the column that is calculated now by this work item,
                 * i.e source is the entry from which a value will be added
                 * to another entry.
                 */
                maintype source = *(b + sourceline);
                /* For the columns the same procedure as for the rows.
                 */
                for (ushort j = sourceline - (sourceline % get_local_size(1u));
                     j < n; j += get_local_size(1u))
                {
                    if (j + get_local_id(1u) > sourceline)
                    {
                        /* tovanish is always the element of the current column
                         * with the row that is calculated now by this work item,
                         * i.e. the entry that shall be nulled now.
                         *
                         * Be aware that here we calculate on b
                         * so we have to "transpose".
                         */
                        maintype tovanish = *(A + n*(j + get_local_id(1u)) + sourceline);
                        *(b + (j + get_local_id(1u))) -= (tovanish/(*pivot))*source;
                    }
                }
            }
            //If the local_id(0u) is greater than the sourceline, we have to calculate on A
            else if (i + get_local_id(0u) > sourceline)
            {
                //Essentially the same as in the other if block.
                for (ushort j = sourceline - (sourceline % get_local_size(1u));
                     j < n; j += get_local_size(1u))
                {
                    //The local_id(1u) must also be greater than sourceline
                    if (j + get_local_id(1u) > sourceline)
                    {
                        maintype tovanish = *(A + n*(i + get_local_id(0u)) + sourceline);
                        maintype source = *(A + n*sourceline + j + get_local_id(1u));
                        *(A + n*(i + get_local_id(0u)) + j + get_local_id(1u)) -=
                            (tovanish/(*pivot))*source;
                    }
                }
            }
        }
    }
    //Now sourceline = n-1 and we go backwards, nulling the upper diagonal entries of A.
    for (; sourceline > 0u; --sourceline)
    {
        /* Now everything is one dimensional (because we only operate on b),
         * so essentially we do the same as above, but with
         * get_local_size(0u)*get_local_size(1u) as the one local size.
         */
        if (get_local_id(0u) == sourceline % get_local_size(0u)
            && get_local_id(1u) == sourceline % get_local_size(1u))
            *pivot = *(A + (n+1u)*sourceline);
        barrier(CLK_LOCAL_MEM_FENCE);

        for (ushort i = 0u; i <= sourceline; i += get_local_size(0u)*get_local_size(1u))
        {
            /* The only difference is that now the "normal" work items 
             * belonging to an entry above sourcelin operate additively on b,
             * while the "special" ones in the current row do the division
             * through the pivot, such that the matrix A is finally the identity.
             */
            if (i + get_local_size(1u)*get_local_id(0u) + get_local_id(1u) == sourceline)
                *(b + sourceline) /= *pivot;
            else if (i + get_local_size(1u)*get_local_id(0u) + get_local_id(1u) < sourceline)
            {
                maintype tovanish = *(A + n*(i + get_local_size(1u)*get_local_id(0u) + get_local_id(1u)) + sourceline);
                maintype source = *(b + sourceline);
                *(b + i + get_local_size(1u)*get_local_id(0u) + get_local_id(1u)) -= (tovanish/(*pivot))*source;
            }
        }
    }
    /* In order to let sourceline unsigned, we leave out the firs row.
     * This needs only one calculating, namely dividing, and this is done
     * now explicitly.
     */
    if (get_local_id(0u) == 0u && get_local_id(1u) == 0u)
        *b /= *A;
}

/**
 * @brief Solves the system of linear equations Ax = b.
 * @param n The size of the matrix A and the vector b.
 * @param A The matrix. Must have size n*n.
 * @param b The vector. Must have size n. 
 * @param dest The destination for the result. Must have size n.
 * @param istoreA A local storage for matrix A. Must have size n*n.
 * @param istoreb A local storage for vector b. Must have size n.
 *
 * Calls linear_solve_dense_loc in order to solve the SLV using
 * Gaussion elimination.
 *
 * The result is stored in the buffer dest.
 * */
__kernel void linear_solve_dense(
    ushort n,
    __global maintype *A, __global maintype *b, __global maintype *dest,
    __local maintype *istoreA, __local maintype *istoreb
)
{
    //Initialize the local variants of A and b.
    for (ushort i = 0u; i + get_local_id(0u) < n; i += get_local_size(0u))
    {
        for (ushort j = 0u; j + get_local_id(1u) < n; j += get_local_size(1u))
        {
            *(istoreA + n*(i + get_local_id(0u)) + j + get_local_id(1u)) =
                *(A + n*(i + get_local_id(0u)) + j + get_local_id(1u));
        }
    }
    for (ushort i = 0u; i + get_local_size(1u)*get_local_id(0u) + get_local_id(1u) < n;
        i += get_local_size(0u)*get_local_size(1u))
    {
        *(istoreb + i + get_local_size(1u)*get_local_id(0u) + get_local_id(1u))
            = *(b + i + get_local_size(1u)*get_local_id(0u) + get_local_id(1u));
    }
    //Declare a local variable for storing the pivot
    __local maintype pivot;
    //Do the solving -- but now, the result is in the local variable istoreb.
    linear_solve_dense_loc(n, istoreA, istoreb, &pivot);
    //So it must me moved to the global variable b.
    for (ushort i = 0u; i + get_local_size(1u)*get_local_id(0u) + get_local_id(1u) < n;
        i += get_local_size(0u)*get_local_size(1u))
    {
        *(dest + i + get_local_size(1u)*get_local_id(0u) + get_local_id(1u))
            = *(istoreb + i + get_local_size(1u)*get_local_id(0u) + get_local_id(1u));
    }
}

#if (0)
bool inc_multiind(size_t *multiind, __constant uint *sizes)
{
    bool further = true;
    for (char dimindx = dimension-1; further && dimindx >= 0; --dimindx)
    {
        ++multiind[dimindx];
        if (multiind[dimindx] < sizes[dimindx])
            further = false;
        else
            multiind[dimindx] = 0;
    }

    return !further;
}



bool dec_multiind(size_t *multiind, __constant uint *sizes)
{
    bool further = true;
    for (char dimindx = dimension-1; further && dimindx >= 0; --dimindx)
    {
        if (multiind[dimindx] > 0)
        {
            --multiind[dimindx];
            further = false;
        }
        else
            multiind[dimindx] = sizes[dimindx] - 1;
    }

    return !further;
}

long calc_work_item_offset(__constant uint *sizes, __constant uint *deltawidth, __constant uint *deltaheight)
{
    long res = 0L;
    long factor = 1L;
    for (char dimindx = dimension-1; dimindx >= 0; --dimindx)
    {
        res += factor * (deltaheight[dimindx] - 1u - get_global_id(dimindx) / deltawidth[dimindx] + get_global_id(dimindx) % deltawidth[dimindx]);
        factor *= (deltawidth[dimindx]);
        res += factor * (get_global_id(dimindx) / (deltawidth[dimindx]));
        factor *= sizes[dimindx];
    }

    return res;
}

long calc_work_item_start_offset(__constant uint *sizes, __constant uint *deltawidth, __constant uint *deltaheight)
{
    long res = 0L;
    long factor = 1L;
    for (char dimindx = dimension-1; dimindx >= 0; --dimindx)
    {
        res += factor * (deltaheight[dimindx] - 1u - get_global_id(dimindx) / deltawidth[dimindx]);
        factor *= (deltawidth[dimindx]);
        res += factor * (get_global_id(dimindx) / (deltawidth[dimindx]));
        factor *= sizes[dimindx];
    }

    return res;
}

long calc_work_item_source_offset(__constant uint *sizes, __constant uint *deltawidth, __constant uint *deltaheight)
{
    long res = 0L;
    long factor = 1L;
    for (char dimindx = dimension-1; dimindx >= 0; --dimindx)
    {
        res += factor * (deltaheight[dimindx] - 1u + get_global_id(dimindx) % deltawidth[dimindx]);
        factor *= (deltawidth[dimindx]);
        factor *= sizes[dimindx];
    }

    return res;
}

long calc_pivot_offset(__constant uint *sizes, __constant uint *deltawidth, __constant uint *deltaheight)
{
    long res = 0L;
    long factor = 1L;
    for (char dimindx = dimension-1; dimindx >= 0; --dimindx)
    {
        res += factor * (deltaheight[dimindx] - 1u);
        factor *= (deltawidth[dimindx]);
        factor *= sizes[dimindx];
    }

    return res;
}

long calc_swap_offset(size_t *swapoffset, __constant uint *sizes, __constant uint *deltawidth, __constant uint *deltaheight)
{
    long res = 0L;
    long factor = 1L;
    for (char dimindx = dimension-1; dimindx >= 0; --dimindx)
    {
        res += factor * (deltaheight[dimindx] - 1u - swapoffset[dimindx]);
        factor *= (deltawidth[dimindx]);
        res += factor * (swapoffset[dimindx]);
        factor *= sizes[dimindx];
    }

    return res;
}

/**
 * @brief works for first and second stage, you must only pass right deltasize
 **/
long calc_work_item_vector_offset(__constant uint *sizes, __constant uint *deltasize)
{
    long res = 0L;
    long factor = 1L;
    for (char dimindx = dimension-1; dimindx >= 0; --dimindx)
    {
        res += factor * (get_global_id(dimindx) / (deltasize[dimindx]));
        factor *= sizes[dimindx];
    }

    return res;
}

long calc_work_item_offset_second_stage(__constant uint *sizes, __constant uint *deltawidth, __constant uint *deltaheight)
{
    long res = 0L;
    long factor = 1L;
    for (char dimindx = dimension-1; dimindx >= 0; --dimindx)
    {
        res += factor * (deltaheight[dimindx] - 1u + get_global_id(dimindx) / deltawidth[dimindx]);
        factor *= (deltawidth[dimindx]);
        res -= factor * (get_global_id(dimindx) / (deltaheight[dimindx]));
        factor *= sizes[dimindx];
    }

    return res;
}

size_t multiind_to_linind_vector(
    size_t * multiind, __constant uint *sizes)
{
    size_t res = 0u;
    long factor = 1L;
    for (char dimindx = dimension-1; dimindx >= 0; --dimindx)
    {
        res += factor * multiind[dimindx];
        factor *= sizes[dimindx];
    }

    return res;
}

size_t multiind_to_linind_istore(
    size_t * multiind, __constant uint *sizes,
    __constant uint *deltawidth, __constant uint *deltaheight)
{
    size_t res = 0u;
    long factor = 1L;
    for (char dimindx = dimension-1; dimindx >= 0; --dimindx)
    {
        factor *= (deltawidth[dimindx]);
        res += factor * multiind[dimindx];
        factor *= sizes[dimindx];
    }

    return res;
}

long delta_to_istore_index(__constant int *delta, __constant uint *deltawidth, __constant uint *deltaheight)
{
    long res = 0L;
    long factor = 1L;
    for (char dimindx = dimension-1; dimindx >= 0; --dimindx)
    {
        res += factor * (deltaheight[dimindx] - 1u - get_global_id(dimindx) + delta[dimindx]);
        factor *= (deltawidth[dimindx]);
        res += factor * get_global_id(dimindx);
        factor *= get_global_size(dimindx);
    }

    return res;
}

/**
 * @attention There must be one delta that is nonpositive, and one that is nonnegative!!!
 **/
void linear_solve_fill_istore(
__constant delta *left, uint leftsize,
__global maintype *istore,
__constant uint *deltawidth, __constant uint *deltaheight)
{
    /*
    long mindelta[dimension], mixdelta[dimension];
    size_t sizes[dimension], multiind[dimension];
    for (uchar dimindx = 0u; dimindx < dimension; ++dimindx)
    {
        mindelta[dimindx] = left[0].del[dimension];
        maxdelta[dimindx] = left[0].del[diminsion];
        for (uint i = 1u; i < leftsize; ++i)
        {
            if (left[i].del[dimension] < mindelta[dimindx])
                mindelta[dimindx] = left[i].del[dimension];
            if (left[i].del[dimension] > maxdelta[dimindx])
                maxdelta[dimindx] = left[i].del[dimension];
        }

        deltaheight[dimension] = -mindelta[dimension] + 1;
        deltawidth[dimension] = 2*(-mindelta[dimension]) + maxdelta[dimension] + 1;

        sizes[dimindx] = get_global_size(dimindx);
        multiind[dimindx] = get_global_id(dimindx);
    }
    */

    size_t index = calc_index();
    for (uint i = 0u; i < leftsize; ++i)
    {
        istore[delta_to_istore_index(left[i].del, deltawidth, deltaheight)] += left[i].val;
    }
}

void linear_solve_delta(
__global maintype *istore,
__global maintype *vector,
__constant uint *sizes,
__constant uint *deltawidth,
__constant uint *deltaheight
)
{
    bool nullitem = true;
    bool vec_item = true;
    size_t multiind[dimension];
    size_t istoresize = 1u, vectorsize = 1u;
    for (uchar dimindx = 0u; dimindx < dimension; ++dimindx)
    {
        if (get_global_id(dimindx) / deltawidth[dimindx] != 0u)
            nullitem = false;
        if (get_global_id(dimindx) % deltawidth[dimindx] != 0u)
            vec_item = false;

        multiind[dimindx] = 0u;
        istoresize *= deltawidth[dimindx];
        istoresize *= sizes[dimindx];
        vectorsize *= sizes[dimindx];
    }

    long pivot_offset = calc_pivot_offset(sizes, deltawidth, deltaheight);
    long work_item_offset = calc_work_item_offset(sizes, deltawidth, deltaheight);
    long start_offset = calc_work_item_start_offset(sizes, deltawidth, deltaheight);
    long source_offset = calc_work_item_source_offset(sizes, deltawidth, deltaheight);
    long vector_offset = calc_work_item_vector_offset(sizes, deltawidth);

    do
    {
        long linind_istore = multiind_to_linind_istore(multiind, sizes, deltawidth, deltaheight);
        maintype pivot = istore[linind_istore + pivot_offset];
        if (pivot == 0.0f)
        {
            if (nullitem)
            {
                size_t swapind[dimension];
                for (uchar dimindx = 0u; dimindx < dimension; ++dimindx)
                {
                    swapind[dimindx] = 0u;
                }
                bool further = true;
                long linind_vector = multiind_to_linind_vector(multiind, sizes);
                while (further && inc_multiind(swapind, deltaheight))
                {
                    long swapoffset = calc_swap_offset(swapind, sizes, deltawidth, deltaheight);
                    if (istore[linind_istore + swapoffset] != 0)
                    {
                        if (vec_item)
                        {
                            vector[linind_vector] += vector[linind_vector + multiind_to_linind_vector(swapind, sizes)];
                        }
                        else
                        {
                            istore[linind_istore + source_offset] +=
                                    istore[linind_istore
                                           + swapoffset
                                           + source_offset];
                        }
                        further = false;
                    }
                }
                if (further)
                {
                    printf("Matrix does not have full rank at %d!", multiind[0]);
                    return;
                }
            }
        }
        if (!nullitem)
        {

            maintype start_pivot_ratio = -istore[linind_istore + start_offset]/istore[linind_istore + pivot_offset];
            if (vec_item)
            {
                long linind_vector = multiind_to_linind_vector(multiind, sizes);
                if (linind_vector + vector_offset <= vectorsize)
                    vector[linind_vector + vector_offset] += vector[linind_vector] * start_pivot_ratio;
            }
            else
            {
                if (linind_istore + work_item_offset <= istoresize)
                    istore[linind_istore + work_item_offset] +=
                            istore[linind_istore + source_offset]*
                            start_pivot_ratio;
            }
        }
        barrier(CLK_GLOBAL_MEM_FENCE);
        if (nullitem)
        {
            if (vec_item)
            {
                long linind_vector = multiind_to_linind_vector(multiind, sizes);
                vector[linind_vector + vector_offset] /= istore[linind_istore + pivot_offset];
            }
            else
                istore[linind_istore + source_offset] /= istore[linind_istore + pivot_offset];
        }
    } while (inc_multiind(multiind, sizes));
    vec_item = true;
    for (uchar dimindx = 0u; dimindx < dimension; ++dimindx)
    {
        if (get_global_id(dimindx) % deltaheight[dimindx] != 0u)
            vec_item = false;

        multiind[dimindx] = deltawidth[dimindx] - 1u;
    }
    work_item_offset = calc_work_item_offset_second_stage(sizes, deltawidth, deltaheight);
    vector_offset = -calc_work_item_vector_offset(sizes, deltaheight);
    barrier(CLK_GLOBAL_MEM_FENCE);
    do
    {
        if (vec_item && !nullitem)
        {
            long linind_istore = multiind_to_linind_istore(multiind, sizes, deltawidth, deltaheight);
            long linind_vector = multiind_to_linind_vector(multiind, sizes);
            if (linind_vector + vector_offset >= 0)
                vector[linind_vector + vector_offset] -= istore[linind_istore + work_item_offset]*vector[linind_vector];
        }
        barrier(CLK_GLOBAL_MEM_FENCE);
    } while (dec_multiind(multiind, sizes));
}


#if (LOC)
void vecmul_delta_loc(__constant delta *left, size_t leftsize,
    __global maintype *right,
    __global maintype *dest,
    __local maintype *right_loc)
{
    size_t index = calc_index();
    size_t index_loc = calc_index_loc();
    maintype res = 0.f;
    right_loc[index_loc] = right[index];
    write_mem_fence(CLK_LOCAL_MEM_FENCE);
    for (size_t i = 0u; i < leftsize; ++i)
    {
        bool in_bounds = 1;
        bool in_bounds_loc = 1;
        for (uchar j = 0u; j < dimension; ++j)
            in_bounds = in_bounds && ((long) get_global_id(j) + left[i].del[j] >= 0L)
                                  && ((long) get_global_id(j) + left[i].del[j] < get_global_size(j));
        if (in_bounds)
        {
            for (uchar j = 0u; j < dimension; ++j)
                in_bounds_loc = in_bounds_loc && ((long) get_local_id(j) + left[i].del[j] >= 0L)
                                      && ((long) get_local_id(j) + left[i].del[j] < get_local_size(j));
            if (in_bounds_loc)
                res += left[i].val * right_loc[index_loc + calc_index_signed_single_loc(left[i].del)];
            else
                res += left[i].val * right[index + calc_index_signed_single(left[i].del)];
        }
    }
    dest[index] = res;
}

void vecmul_delta_vec_loc(__constant delta *left, size_t leftsize,
    __global maintype *right,
    __global maintype *dest,
    __local mainvec *right_loc)
{
    size_t index = calc_index();
    size_t index_loc = calc_index_loc();
    mainvec res = (mainvec)(0.f);
    right_loc[index_loc] = VLOAD_FUNC(index, right);
    write_mem_fence(CLK_LOCAL_MEM_FENCE);
    for (size_t i = 0u; i < leftsize; ++i)
    {
        int in_bounds = 1;
        for (uint j = 0u; in_bounds && j < get_work_dim()-1; ++j)
            in_bounds = in_bounds && ((long) get_global_id(j) + left[i].del[j] >= 0)
                                  && ((long) get_global_id(j) + left[i].del[j] < get_global_size(j));
        int in_bounds_loc = in_bounds;
        for (uint j = 0u; in_bounds_loc && j < get_work_dim()-1; ++j)
            in_bounds_loc = in_bounds_loc && ((long) get_local_id(j) + left[i].del[j] >= 0)
                                  && ((long) get_local_id(j) + left[i].del[j] < get_local_size(j));
        if (in_bounds)
        {
            mainvec rightvec;
            uint lastdim = get_work_dim() - 1u;
            if ((left[i].del[lastdim] & (mainvecsize - 1u)) == 0u)
            {
                if (in_bounds_loc)
                    rightvec = right_loc[index_loc + calc_index_from_signed_vec_loc(left[i].del)];
                else
                    rightvec = VLOAD_FUNC(index + calc_index_from_signed_vec(left[i].del), right);
            }
            else
            {
                mainvec rightvec1, rightvec2;
                int del = left[i].del[lastdim];
                uint delun = (uint)(del) & (mainvecsize - 1u);
                if (in_bounds_loc)
                {
                    rightvec1 =
                        (((long) (get_local_id(lastdim)*mainvecsize) + del >= 0L) &&
                         ((long) (get_local_id(lastdim)*mainvecsize) + del
                          < get_local_size(lastdim)*mainvecsize))?
                            right_loc[index_loc + calc_index_from_signed_vec_loc(left[i].del)] :

                            ((((long) (get_global_id(lastdim)*mainvecsize) + del >= 0L) &&
                              ((long) (get_global_id(lastdim)*mainvecsize) + del
                                    < get_global_size(lastdim)*mainvecsize))?
                            VLOAD_FUNC((long)(index) + calc_index_from_signed_vec(left[i].del), right) : (0.f));
                    rightvec2 =
                        (((long) ((get_local_id(lastdim) + 1L)*mainvecsize) + del >= 0L) &&
                         ((long) ((get_local_id(lastdim) + 1L)*mainvecsize) + del
                          < get_local_size(lastdim)*mainvecsize))?
                        right_loc[index_loc + calc_index_from_signed_vec_loc(left[i].del) + 1] :

                        ((((long) ((get_global_id(lastdim) + 1L)*mainvecsize) + del >= 0L) &&
                          ((long) ((get_global_id(lastdim) + 1L)*mainvecsize) + del
                           < get_global_size(lastdim)*mainvecsize))?
                        VLOAD_FUNC((long)(index) + calc_index_from_signed_vec(left[i].del) + 1, right) : (0.f));
                }
                else
                {
                    bool in_bounds_left =
                            ((long) (get_global_id(lastdim)*mainvecsize) + del
                                    >= 0L) &&
                            ((long) (get_global_id(lastdim)*mainvecsize) + del
                                    < get_global_size(lastdim)*mainvecsize);
                    bool in_bounds_right =
                            ((long) ((get_global_id(lastdim) + 1L)*mainvecsize) + del
                                    >= 0L) &&
                            ((long) ((get_global_id(lastdim) + 1L)*mainvecsize) + del
                                    < get_global_size(lastdim)*mainvecsize);
                    rightvec1 = (in_bounds_left)?
                            VLOAD_FUNC((long)(index) + calc_index_from_signed_vec(left[i].del), right) : (0.f);
                    rightvec2 = (in_bounds_right)?
                            VLOAD_FUNC((long)(index) + calc_index_from_signed_vec(left[i].del) + 1, right) : (0.f);
                }

#if (VECSIZE == 16u)
                mainint mask2;
                mask2.even = (shufflemask_half.lo
                              + (uint) ((mainvecsize >> 0x1u) * ((uint)(del) & 0x1u)));
                mask2.odd  = (shufflemask_half.hi
                              + (uint) ((mainvecsize >> 0x1u) * ((uint)(del) & 0x1u)));
                rightvec = shuffle2(
                    shuffle2(rightvec1.even, rightvec2.even,
                             shufflemask_half
                             + (uint) ((((uint)(del) & (mainvecsize - 1u))+1u) >> 0x1u)).lo,
                    shuffle2(rightvec1.odd , rightvec2.odd,
                             shufflemask_half
                             +  (uint) (((uint)(del) & (mainvecsize - 1u)    ) >> 0x1u)).lo,
                    mask2);
#else
                rightvec =
                       shuffle2(rightvec1, rightvec2,
                                  shufflemask + (uint) (del & (mainvecsize - 1u))).lo;
#endif//VECSIZE
                /*if (get_global_id(0) >= 12 && get_global_id(0) < 14
                && get_global_id(1) >= 12 && get_global_id(1) < 14)
                printf("i: %d; index: (%d, %d); rightvec1: %0.0v4hlf; rightvec2: %0.0v4hlf; rightvec: %0.0v4hlf; offset: %d\n",
                                i, get_global_id(0), get_global_id(1),
                                rightvec1, rightvec2,
                                rightvec, calc_index_from_signed_vec_loc(left[i].del));*/
            }
            /*if (get_global_id(0) >= 12 && get_global_id(0) < 14
            && get_global_id(1) >= 12 && get_global_id(1) < 14)
            printf("i: %d; index: (%d, %d);\nrightvec: %0.0v4hlf;\nres: %0.0v4hlf;\nres_after: %0.0v4hlf;\n\n",
            i, get_global_id(0), get_global_id(1),
            rightvec, res, res + left[i].val * rightvec);*/
            res += left[i].val * rightvec;
        }
    }
    VSTORE_FUNC(res, index, dest);
}
#endif//LOC
#endif//0
